import { defineConfig } from 'umi';

export default defineConfig({
  dva: {},
  fastRefresh: true,
  title: "昆仑管理系统",
  routes: [
    { path: '/', component: '@/pages/LoginPage' },
    { path: '/kunlun', component: '@/pages/AppPage' },
  ],
});
