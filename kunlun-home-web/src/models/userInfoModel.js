import * as globalService from '../services/globalService';

export default {
  namespace: "userInfoModel",
  state: {
    noficationList: [],
    messageList: [],
    todoList: [],
    badgeCount: 0,
    userInfoData: null,
  },
  reducers: {
    updateState(state, { payload }) {
      return { ...state, ...payload }
    }
  },
  effects: {
    *onDetail({ payload: params }, { select, call, put }) {
      let { paneTabs } = yield select(state => state.globalModel);
      let {activeHeadMenuKey, paneTabList} = yield call(globalService.getActivedMenu, params, paneTabs);
      console.log("open tab, activeHeadMenuKey ===>>> " + activeHeadMenuKey + ", activeSideMenuKey ===>>> " + params.key);
      yield put({type: "globalModel/updateState", payload: {activeHeadMenuKey, activeSideMenuKey: params.key, paneTabs}});
    },
  },
  subscriptions: {
    setup({ dispatch, history }) {
      const pathname = window.location.pathname;
      console.log("userInfoModel subscriptions setup => ", pathname);
    },
  },
};
