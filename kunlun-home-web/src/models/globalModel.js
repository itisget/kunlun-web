import React from 'react';
import config from "../config/config";
import { message, Modal } from 'antd';
import * as globalService from '../services/globalService';

export default {
  namespace: "globalModel",
  state: {
    collapsed: false,
    activeHeadMenuKey: "home",
    activeSideMenuKey: "platform",
    menuData: null,
    pageUrl: null,
    pathUrlList: [],
    paneTabs: [config.frame_menu.sider.home[0]],
    openedSubMenuKey: null,
    homeView: null,
    tokenModel: null,
    codeModel: null,
    pageLoading: false,
    themeDrawerVisible: false,
    themeColor: config.DEFAULT_THEME_COLOR,
    siderColor: config.DEFAULT_SIDER_COLOR,
    themeStyle: "sideMenu",
    selectedStyle: "theme",
    isBlockStyle: true,
    menuMap: new Map(),
    timeoutModalCount: 0,
    userInfoVisible: false,
    systemStatus: "login",
  },
  reducers: {
    updateState(state, { payload }) {
      return { ...state, ...payload }
    }
  },
  effects: {
    *getAuthCode({ payload: params }, { select, call, put }) {
      const res = yield call(globalService.getAuthCode, params);
      if (res.code == "200") {
        console.log("Authentication Code ===> " + JSON.stringify(res.data));
        yield put({ type: "updateState", payload: { codeModel: res.data, systemStatus: "login" }});
      }
    },

    *login({ payload: params }, { select, call, put }) {
      console.log("===== globalModel login =====");
      yield put({ type: "updateState", payload: { pageLoading: true }});
      const { userName, password } = params;
      const {codeModel} = yield select(state => state.globalModel);
      const res = yield call(globalService.login, { userName, password, code: codeModel.code });
      console.log("===== globalModel login res ===== " + res.code);
      if (res.code == "200") {
        console.log(res);
        window._USERINFO_ = res.data.userInfo;
        window._TOKEN_ = res.data.token;
        sessionStorage.token = window._TOKEN_;
        sessionStorage.userInfo = JSON.stringify(window._USERINFO_);
        yield put({ type: "updateState", payload: { tokenModel: res.data }});
        yield put.resolve({ type: "getAppMenu", payload: {userId: res.data.userInfo.id}});
        window.location.href = "http://localhost:8000/kunlun";
      } else {
        message.error(res.message);
        console.log(res.error);
      }
      yield put({ type: "updateState", payload: { pageLoading: false, systemStatus: "run" }});
    },

    *getAppMenu({ payload: params }, { select, call, put }) {
      const res = yield call(globalService.getAppMenu, params);
      if (res.code == "200") {
        console.log("getAppMenu menuData ===> " + res.data);
        sessionStorage.menuData = JSON.stringify(res.data);
        yield put({ type: "updateState", payload: { menuData: res.data }});
      } else {
        console.log("config menuData ===> " + res.data);
        const menuList = yield call(globalService.getAppMenuFromConfig);
        let menuData = {};
        menuData["list"] = menuList;
        menuData = {...menuData, ...config.frame_menu};
        sessionStorage.menuData = JSON.stringify(menuData);
        yield put({ type: "updateState", payload: { menuData }});
      }
    },

    *logout({ payload: params }, { select, call, put }) {
      yield put({ type: "updateState", payload: { pageLoading: true }});
      const { userName, password } = window._USERINFO_ ? JSON.parse(window._USERINFO_) : JSON.parse(sessionStorage.userInfo);
      const res = yield call(globalService.logout, { userName, password });
      if (res.code == "200") {
        console.log("homeModel logout res => ", res);
      }
      yield put({ type: "updateState", payload: { pageLoading: false, paneTabs: [], activeHeadMenuKey: "home", activeSideMenuKey: null, systemStatus: "logout" }});
    },

    *addActiveRoute({ payload: params }, { select, call, put }) {
      let { menuData, paneTabs } = yield select(state => state.globalModel);
      const {activeHeadMenuKey, paneTabList } = yield call(globalService.getActivedMenu, params, paneTabs, menuData.sider);
      console.log("open tab, activeHeadMenuKey ===>>> " + activeHeadMenuKey + ", activeSideMenuKey ===>>> " + params.key);
      yield put({ type: "updateState", payload: { paneTabs: paneTabList, activeHeadMenuKey, activeSideMenuKey: params.key }});
    },

    *refreshPage({payload: params}, {select, call, put}) {
      const tokenModel = {};
      const token = sessionStorage.getItem("token");
      window._TOKEN_ = token;
      const userInfo = sessionStorage.getItem("userInfo");
      window._USERINFO_ = userInfo;
      tokenModel["token"] = token;
      tokenModel["userInfo"] = userInfo;
      let menuData = !!sessionStorage.getItem("menuData") ? JSON.parse(sessionStorage.getItem("menuData")) : null;
      // 兼容单独运行前端服务
      if (!menuData) {
        const menuList = globalService.getAppMenuFromConfig();
        menuData = {};
        menuData["list"] = menuList;
        menuData = {...menuData, ...config.frame_menu};
      }

      debugger

      yield put({type: "updateState", payload: {tokenModel, menuData}});
    },

    *handleTimeout({payload: params}, {select, call, put}) {
      const {dispatch} = params;
      let {timeoutModalCount} = yield select(state => state.globalModel);
      if (timeoutModalCount > 0) return;
      timeoutModalCount++;

      Modal.confirm({
        title: '提示',
        okText: "确认",
        cancelText: "取消",
        content: "离开时间太长，请重新登录！",
        onOk() {
          dispatch({type: "logout", payload: {}}).then(() =>
            window.location.href = "http://localhost:8000"
          );
        },
        onCancel() {
          timeoutModalCount--;
          dispatch({type: "updateState", payload: {timeoutModalCount}});
        },
      });
      dispatch({type: "updateState", payload: {timeoutModalCount}});
    },
  },
  subscriptions: {
    onListenIFrameMessage({ dispatch, history }) {
      window.addEventListener("message", function (e) {
        // 监听页面超时事件，确定后直接跳转到登陆界面
        console.log("globalModel Listener e => ", e);
        if (e.data && e.data.operateType == "timeout") {
          dispatch({type: "handleTimeout", payload: { dispatch }});
        } else if (!!e && !!e.data && !(e.data instanceof Object)) {
          // kunlun-system-web请求打开菜单页面的监听
          const message = !!e && !!e.data && JSON.parse(e.data) || {};
          message.isAddRoute && dispatch({type: "addActiveRoute", payload: message});
        }
      });
    },
    setup({ dispatch, history }) {
      const pathname = window.location.pathname;
      console.log("homeModel subscriptions setup pathname => ", pathname);
      if (pathname == "/") {
        dispatch({type: "getAuthCode", payload: {}});
      }

      // 刷新页面处理
      if (pathname == "/kunlun") {
        dispatch({type: "refreshPage", payload: {}});
      }
    },
  },
};
