import React from 'react';

export function sendRequestToHome(isAddRoute, key, params) {
  window.parent.postMessage(JSON.stringify({ isAddRoute, key, params }), "*");
}
