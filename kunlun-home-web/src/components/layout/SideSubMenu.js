import React from 'react';
import {Menu} from 'antd';

const { SubMenu } = Menu;
const MenuItem = Menu.Item;

/**
 * 左侧菜单及中间内容显示
 */
class SideSubMenu extends React.Component {

  constructor(props) {
    super(props);
  }

  render() {

    const { collapsed, activeHeadMenuKey, onSelectSideMenu, activeSideMenuKey, menuData,
      onOpenSubMenu, openedSubMenuKey, siderColor
    } = this.props;

    const iconStyle = (iconType) => {
      return {
        paddingRight: iconType == "dropdown" ? "5px" : collapsed ? "15px" : "5px",
        fontSize: "16px",
      }
    };

    // 左侧菜单
    const siderFlag = menuData.sider[activeHeadMenuKey] ? true : false;
    const siderMenu = siderFlag ? menuData.sider[activeHeadMenuKey].filter(item => item.show).map(item => (item.children ?
      <SubMenu
          key={item.key}
          title={<span><i class={item.icon} style={iconStyle("submenu")}/><span style={{ padding: "0px 0px 0px 0px" }}>{item.name}</span></span>} style={{ background: siderColor }}
      >
        {
          item.children ? item.children.filter(item => item.isShow).map(subItem => (
            <MenuItem key={subItem.key} path={subItem.url}>
              <i className={subItem.icon} style={iconStyle("submenu")}/><span style={{ padding: "0px 0px 0px 0px" }}>{subItem.name}</span>
            </MenuItem>)) : ""
        }
      </SubMenu> :
      <MenuItem key={item.key} path={item.url}>
        <i className={item.icon} style={iconStyle("submenu")}/><span style={{ padding: "0px 0px 0px 0px" }}>{item.name}</span>
      </MenuItem>)) : "";

    return (
      <div style={{height: "100%"}}>
        <Menu
          mode="inline"
          onClick={onSelectSideMenu}
          openKeys={openedSubMenuKey}
          onOpenChange={onOpenSubMenu}
          selectedKeys={activeSideMenuKey}
          style={{height: '100%', background: siderColor}}
        >
          {/* 引入左侧菜单 */}
          {siderMenu}
        </Menu>
      </div>
    );
  };
}

export default SideSubMenu;
