import React from 'react';
import {Menu, Tabs, Dropdown, Tooltip} from 'antd';
import styles from './Layout.less';

const MenuItem = Menu.Item;
const TabPane = Tabs.TabPane;

/**
 * 左侧菜单及中间内容显示
 */
class ContentTabFrame extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      refreshView: null
    }
  }

  componentWillReceiveProps(nextProps) {
    const {tokenModel, themeColor, systemStatus} = nextProps;
    this.onLoadIFrame(tokenModel, themeColor, systemStatus);
  }

  componentDidUpdate(prevProps, prevState, snapshot) {
    const {refreshView} = this.state;
    if (!!refreshView) {
      this.setState({refreshView: null});
    }
  }

  onLoadIFrame(tokenModel, themeColor, systemStatus) {
    if (systemStatus == "logout") {
      console.log("onLoadIFrame systemStatus ===>>> " + systemStatus);
      return;
    }

    // 传递参数到子IFrame页面进行交互
    const iFrameParams = { token: tokenModel && tokenModel.token, userInfo: tokenModel && tokenModel.userInfo, themeColor, isAuth: true };
    const index = window.frames ? window.frames.length - 1 : 0;
    console.log("onLoadIFrame window.frames ===>>> " + (window.frames ? true : false) + " index ===>>> " + index);
    const postMessageObject = window.frames[index] ? window.frames[index] : window.frames;
    postMessageObject.postMessage(iFrameParams, '*');
  }

  render() {

    const { collapsed, activeHeadMenuKey, activeSideMenuKey, paneTabs, onTabChange, removeTab,
      closeCurrentTab, closeOtherTab, onCloseTab, tokenModel, themeColor, systemStatus
    } = this.props;

    const {refreshView} = this.state;

    const iconStyle = (iconType) => {
      return {
        paddingRight: iconType == "dropdown" ? "5px" : collapsed ? "32px" : "5px",
        fontSize: "16px",
      }
    };

    // Tabs页签编辑，即删除
    const onEdit = (targetKey, type) => {
      if (type != "add") {
        removeTab(targetKey, activeSideMenuKey);
      }
    };

    const dropdownOptions = (
      <Menu style={{ marginTop: "10px" }}>
        <MenuItem style={{ marginTop: "3px", marginLeft: "5px" }}>
          <div onClick={() => onRefreshTab()}>
            <i className="ri-refresh-line" style={iconStyle("dropdown")}></i>&nbsp;刷新当前页签
          </div>
        </MenuItem>
        <MenuItem style={{ marginTop: "3px", marginBottom: "5px", marginLeft: "5px" }}>
          <div onClick={closeCurrentTab}>
            <i className="ri-close-line" style={iconStyle("dropdown")}></i>&nbsp;关闭当前页签
          </div>
        </MenuItem>
        <MenuItem style={{ marginTop: "3px", marginBottom: "5px", marginLeft: "5px" }}>
          <div onClick={closeOtherTab}>
            <i className="ri-checkbox-indeterminate-line" style={iconStyle("dropdown")}></i>&nbsp;关闭其他页签
          </div>
        </MenuItem>
        <MenuItem style={{ marginTop: "3px", marginBottom: "5px", marginLeft: "5px" }}>
          <div onClick={onCloseTab}>
            <i className="ri-close-circle-line" style={iconStyle("dropdown")}></i>&nbsp;关闭所有页签
          </div>
        </MenuItem>
      </Menu>
    );

    const tabOperateOptions = <div style={{ padding: "5px 10px 0px 10px" }}>
      <Dropdown overlay={dropdownOptions} trigger={"click"} overlayClassName={styles.cursorDiv}>
        <Tooltip title={"页签操作"} placement={"left"}>
          <i className="ri-menu-line" style={{ fontSize: "20px" }}></i>
        </Tooltip>
      </Dropdown>
    </div>;

    // 刷新当前页签
    const onRefreshTab = () => {
      this.setState({ refreshView: new Date().getTime() });
    };

    const handlePageUrl = (pane) => {
      return pane.url + (refreshView && activeSideMenuKey == pane.key ? ("?refreshView=" + refreshView + "&") : "?");
    }

    const createTabItems = () => {
      let tabItems = new Array();
      const widthValue = window.innerWidth - (collapsed ? 55 : 200);
      const heightValue = window.innerHeight - 101;
      for (let i = 0; i < paneTabs.length; i++) {
        const item = paneTabs[i];
        const iframeDom = <iframe id={"tabIFrame_" + activeHeadMenuKey + "_" + activeSideMenuKey + "_" + i}
                                  name={"tabIFrame_" + activeHeadMenuKey + "_" + activeSideMenuKey + "_" + i}
                                  style={{ width: widthValue, height: heightValue }}
                                  onLoad={() => this.onLoadIFrame(tokenModel, themeColor, systemStatus)}
                                  src={ handlePageUrl(item) } frameBorder={"no"}
                          />;
        tabItems.push({ key: item.key, label: item.name, closable: "platform" != item.key, children: iframeDom });
      }
      return tabItems;
    }

    const widthValue = window.innerWidth - (collapsed ? 55 : 200);
    return (
      <div style={{ height: "100%", background: "#fff", width: widthValue }}>
        <Tabs
          hideAdd
          onEdit={onEdit}
          tabBarGutter={1}
          type="editable-card"
          onChange={onTabChange}
          items={ createTabItems() }
          activeKey={activeSideMenuKey}
          tabBarExtraContent={tabOperateOptions}
          className={[styles.gapTab, styles.distanceDiv]}
          tabBarStyle={{  }}
        />
      </div>
    );
  };
}

export default ContentTabFrame;
