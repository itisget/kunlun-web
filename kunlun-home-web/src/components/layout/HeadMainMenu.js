import React from 'react';
import {Menu, Dropdown, Avatar, Divider, Tooltip, Badge} from 'antd';
import styles from './Layout.less';
import config from "../../config/config";
import NoficationPane from '../nofication/NoficationPane';
import moment from 'moment';

const MenuItem = Menu.Item;
const MenuSubMenu = Menu.SubMenu;

/**
 * 顶部主菜单及其内容
 */
class HeadMainMenu extends React.Component {

  constructor(props) {
    super(props);
  }

  render() {

    const { collapsed, activeHeadMenuKey, toggleSiderMenu, onSelectHeadMenu, menuData,
      noficationList, messageList, todoList, onShowUserInfo, onLogout, tokenModel, onSystemInfo,
      onTheme, themeStyle, themeColor, badgeCount, clearPane, onDetail
    } = this.props;

    const iconStyle = {
      color: "#ffffff",
      paddingRight: "5px",
      fontSize: "16px",
    }

    const iconStyleSubMenu = {
      color: "#070707",
      paddingRight: "5px",
      fontSize: "16px",
      verticalAlign: "sub",
    }

    const iconStyleUserInfo = {
      paddingRight: "5px",
      fontSize: "16px",
      verticalAlign: "sub",
    }

    // 主菜单项
    const initMainMenu = () => {
      const main = menuData.main;
      const list = menuData.list;
      let mainMenuList = [];
      if (themeStyle == "sideMenu") {
        mainMenuList =
          main.map(item =>
            <MenuItem key={item.key} path={item.url}>
              <i className={item.icon} style={iconStyle} />
              <span style={{color: "#ffffff", fontSize: "15px"}}>{item.name}</span>
            </MenuItem>);
      } else if (themeStyle == "subMenu") {
        mainMenuList = main.map(item => {
          let subMenuList;
          let filters = list.filter(obj => obj.longCode.indexOf(item.id) > -1 && obj.id != item.id);
          if (!filters || !!filters && filters.length == 0) {
            subMenuList =
              <MenuItem key={item.key} path={item.url}>
                <i className={item.icon} style={iconStyle} />
                <span style={{color: "#ffffff", fontSize: "15px"}}>{item.name}</span>
              </MenuItem>;
          } else {
            const itemNameDom =
              <>
                <i className={item.icon} style={iconStyle} />
                <span style={{color: "#ffffff", fontSize: "15px"}}>{item.name}</span>
              </>;
            subMenuList =
            <MenuSubMenu key={item.key} path={item.url} title={itemNameDom} popupClassName={styles.sub_menu_ul}>
              {
                filters.map(subItem =>
                  <MenuItem key={subItem.key} path={subItem.url}>
                    <i className={subItem.icon} style={iconStyleSubMenu} />
                    <span style={{color: "rgba(0, 0, 0, 0.85)", fontSize: "15px"}}>{subItem.name}</span>
                  </MenuItem>)
              }
            </MenuSubMenu>;
          }
          return subMenuList;
        });
      }
      return mainMenuList;
    };

    // 消息通知项
    const noficationPaneProps = { noficationList, messageList, todoList, clearPane, onDetail };
    const dropdownNoficationOptions = (
      <NoficationPane {...noficationPaneProps} />
    );

    // 个人信息项
    const dropdownInfoOptions = (
      <Menu style={{ marginTop: "-8px" }}>
        <div style={{background: themeColor, height: "165px", textAlign: "center", padding: "15px 0px 10px 0px"}}>
          <Avatar size={80} icon={<i className={"ri-user-line"} />} />
          <div style={{marginTop: "10px"}}>
            <span style={{fontWeight: "bolder"}}>admin</span>
            <span style={{marginLeft: "20px"}}>系统管理员</span>
            <div>15555555555</div>
            <div>{moment(new Date()).format("YYYY-MM-DD HH:mm:ss")}</div>
          </div>
        </div>
        <MenuItem style={{ margin: "5px 10px 0px 10px" }}>
          <div onClick={onShowUserInfo}><i className="ri-shield-user-line" style={iconStyleUserInfo}></i>&nbsp;个人中心</div>
        </MenuItem>
        <Divider style={{ margin: "5px" }} />
        <MenuItem style={{ margin: "5px 10px 0px 10px", marginBottom: "10px" }}>
          <div onClick={onLogout}><i className="ri-logout-circle-r-line" style={iconStyleUserInfo}></i>&nbsp;退出系统</div>
        </MenuItem>
      </Menu>
    );

  const userInfo = tokenModel && tokenModel.userInfo ? JSON.parse(tokenModel.userInfo) : "";

  const createSystemToolBar = () => {
    return (
      <ul className={styles.head_menu_tool_bar}>
        <li className={styles.menuToolBarliMessage}>
          <Tooltip title={"消息待办"}>
            <Dropdown overlay={dropdownNoficationOptions} trigger={['click']} overlayClassName={styles.noticeInfoDiv}>
                <Badge dot={badgeCount > 0 ? true : false}>
                  <i className="ri-volume-up-line" style={{ fontSize: "18px", color: "#ffffff", verticalAlign: "text-bottom" }}></i>
                </Badge>
            </Dropdown>
          </Tooltip>
        </li>
        <li className={styles.menuToolBarli} onClick={onTheme}>
          <Tooltip title={"主题样式"}>
            <i className="ri-t-shirt-line" style={{ fontSize: "18px", color: "#ffffff", verticalAlign: "text-bottom" }}></i>
          </Tooltip>
        </li>
        <li className={styles.menuToolBarli} onClick={onSystemInfo}>
          <Tooltip title={"了解项目"}>
            <i className="ri-information-line" style={{ fontSize: "18px", color: "#ffffff", verticalAlign: "text-bottom" }}></i>
          </Tooltip>
        </li>
        <li className={styles.user_avatar_span}>
          <Dropdown overlay={dropdownInfoOptions} trigger={['click']} overlayClassName={styles.personInfoDiv}>
            <Tooltip title={"个人中心"} style={{ marginTop: "-10px" }}>
              <Avatar icon={<i className="ri-account-circle-line" style={{fontSize: "19px"}}></i>} style={{ marginTop: "16px", background: "#096dd9" }} />
              {/*<span style={{ color: "#ffffff" }}>&nbsp;{userInfo ? userInfo.userName : "admin"}</span>*/}
            </Tooltip>
          </Dropdown>
        </li>
      </ul>
    );
  }

  return (
    <div style={{ height: "55px", display: "flex" }}>
      <div style={{ width: "250px", lineHeight: "55px" }}>
        <i className="ri-global-fill" style={{ fontSize: "35px", color: "aliceblue", marginLeft: "-5%" }} />
        <span className={styles.logContent}>{config.name}</span>
      </div>
      <div title={"缩放左侧菜单"} onClick={e => {toggleSiderMenu()}} className={styles.collapseDiv}>
        <i className={collapsed ? "ri-indent-increase" : "ri-indent-decrease"} />
      </div>
      <Menu
        className={styles.menu}
        selectedKeys={[activeHeadMenuKey]}
        theme="dark"
        mode="horizontal"
        style={{ width: "100%",height: "55px", marginLeft: "-50px", background: themeColor }}
        onClick={onSelectHeadMenu}
      >
        {/* 引入主菜单 */}
        { initMainMenu() }
      </Menu>

      {/* 主菜单右侧状态栏 */}
      { createSystemToolBar() }
    </div>
  )};
}

export default HeadMainMenu;
