import React from 'react';
import styles from './Login.less';
import config from '../../config/config';
import {Button, Form, Input, Row, message, Spin} from 'antd';

const FormItem = Form.Item;

const Login = (props) => {

  const {codeModel, onLogin, refreshCode, pageLoading} = props;

  const [form] = Form.useForm();
  const {getFieldsValue, validateFields, resetFields} = form;

  const login = () => {
    validateFields().then(values => {
      if (values["code"].toLowerCase() != codeModel.code.toLowerCase()) {
        message.warning("输入的验证码有误！");
        return;
      }

      if (new Date().getTime() - new Date(codeModel.createTime).getTime() > 1000 * 60) {
        message.warning("验证码已过期，请重新输入！");
        resetFields("code");
        return;
      }
      onLogin(values);
    }).catch(error => {
      console.log("===== 登陆验证失败 =====");
    });
  };

  const onKeyEnter = (e) => {
    if (e.keyCode === 13) {
      login()
    }
  };

  const createLoginForm = () => {
    return (
      <Form initialValues={{}} form={form}>
        <Row align="center">
          <div className={styles.loginFont}>用户登录</div>
        </Row>
        <Row align="center">
          <FormItem name={"userName"} rules={[{required: true, message: "请输入用户名!"}]}>
            <Input style={{ width: "300px" }} placeholder={"请输入用户名"} prefix={<i className="ri-user-3-line" style={{ color: '#506c86' }}></i>}/>
          </FormItem>
        </Row>
        <Row align="center">
          <FormItem name={"password"} rules={[{ required: true, message: "请输入密码!" }]}>
            <Input.Password style={{ width: "300px" }} placeholder={"请输入密码"} prefix={<i className="ri-lock-password-line" style={{ color: '#506c86' }}></i>}/>
          </FormItem>
        </Row>
        <Row align="center" style={{ marginLeft: "-150px" }}>
          <FormItem name={"code"} rules={[{ required: true, message: "请输入验证码!" }]}>
            <Input placeholder={"请输入验证码"} style={{ width: "150px" }} onPressEnter={onKeyEnter} prefix={<i className="ri-shield-flash-line" style={{ color: '#506c86' }}></i>} />
          </FormItem>
        </Row>
        <Row style={{ marginTop: "-56px", float: "right" }}>
          <img onClick={refreshCode} src={codeModel ? "data:image/png;base64," + codeModel.binary : "default.png"} style={{height: "32px", width: "120px"}}/>
        </Row>
        <Row align="center">
          <Button type="primary" style={{ width: "100%" }} onClick={login}>登录</Button>
        </Row>
      </Form>
    );
  }

  return (
    <div id={"loginDiv"} style={{ width: "100%", height: '100%' }}>
      <Spin spinning={pageLoading} size={"large"} tip={"数据加载中，请稍候！"} style={{ width: "100%", height: '100%' }}>
        <div style={{ display: "flex", padding: "100px", top: 0, bottom: 0, left: 0, right: 0, position: "fixed" }}>
          <div style={{ background: "#ffffff", padding: "45px", width: "100%", height: '100%' }}>
            <div style={{ position: "absolute", padding: "12px 0px 0px 8px" }}>
              <i className="ri-global-fill" style={{ fontSize: "35px", marginLeft: "3px", marginTop: "-10px", float: "left" }}></i>
              <div className={styles.logoFont}>{config.name}</div>
            </div>
            <img src={require("../../assets/login.jpg")} style={{ background: "#ffffff", width: "100%", height: "100%" }} />
          </div>
          <div style={{ background: "#ffffff", padding: "45px 45px 45px 0px" }}>
            <div className={styles.welcomeDiv}>欢迎使用 KunLun Admin</div>
            <div style={{ height: "100%", widht: "100%", paddingTop: (window.innerHeight - 620) / 2 }}>
              { createLoginForm() }
            </div>
            <div className={styles.login_footer_copyright_div}>{config.footerText}</div>
          </div>
        </div>
      </Spin>
    </div>
  );
};

export default Login;
