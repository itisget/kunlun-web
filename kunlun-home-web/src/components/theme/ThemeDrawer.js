import React from 'react';
import {Radio, Drawer, Button, Tabs} from 'antd';
import styles from './Theme.less';
import { SketchPicker } from 'react-color';
import config from '../../config/config';

const RadioGroup = Radio.Group;
const TabPane = Tabs.TabPane;

const ThemeDrawer = (props = {}) => {

  // 从传递过来的props中获取参数
  const {
    themeDrawerVisible, onClose, onChangeColor, onDefaultColor, themeColor, onSelectStyle, selectedStyle, siderColor
  } = props;

  const iconStyle = {
    verticalAlign: "sub",
    marginRight: "5px",
    fontSize: "17px"
  };

  return (
    <div id={"themeDrawerDiv"}>
      <Drawer
        mask={false}
        className={styles.drawer}
        width={280}
        placement="right"
        onClose={onClose}
        visible={themeDrawerVisible}
        mask={false}
      >
        <Tabs hideAdd onChange={() => {}} type="editable-card" className={styles.tabPane}>
          <TabPane tab={"主题颜色"} key={"color"} closable={false} className={styles.sketchPicker} style={{ paddingLeft: "15px" }}>
            <div style={{paddingBottom: "5px"}}>
              <div style={{width: "89%"}}>
                <i className="ri-list-settings-fill" style={iconStyle}></i>
                <span style={{marginLeft: "5px", fontWeight: "bolder"}}>预置：</span>
              </div>
              <div>
                <RadioGroup onChange={(e) => onChangeColor("preset", e.target.value)} value={themeColor} style={{ marginBottom: "10px", lineHeight: "2" }}>
                  {
                    config.theme_color.map(item => <Radio key={item.key} value={item.value} style={{color: item.value}}>{item.name}</Radio>)
                  }
                </RadioGroup>
              </div>
              <div>
                <div style={{width: "89%"}}>
                  <i className="ri-edit-box-fill" style={iconStyle}></i>
                  <span style={{marginLeft: "5px", fontWeight: "bolder"}}>自定义：</span>
                </div>
                <div>
                  <RadioGroup onChange={onSelectStyle} value={selectedStyle} style={{ marginBottom: "10px" }}>
                    <Radio value={"theme"}>主题栏</Radio>
                    <Radio value={"sider"}>侧边栏</Radio>
                  </RadioGroup>
                  <SketchPicker style={{ marginBottom: "15px" }} color={selectedStyle == "theme" ? themeColor : siderColor} onChangeComplete={(e) => onChangeColor("select", e.hex)} />
                </div>
              </div>
            </div>
            <Button type={"primary"} icon={<i className="ri-save-3-line" style={iconStyle}></i>} onClick={onDefaultColor} style={{ width: "250px", marginTop: "10px" }}>恢复默认</Button>
          </TabPane>
        </Tabs>
      </Drawer>
    </div>
  );
}

export default ThemeDrawer;
