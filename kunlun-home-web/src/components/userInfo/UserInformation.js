import React, { Component, useState } from 'react';
import { Divider, Avatar, Form, Col, Tag, Tree } from 'antd';
import styles from './UserInfo.less';
import 'remixicon/fonts/remixicon.css';
import moment from 'moment';

const TreeNode = Tree.TreeNode;

/**
 * 顶部主菜单及其内容
 */
const UserInformation = (props) => {

  const {tokenModel, userInfoData} = props;

  const iconStyle = {
    verticalAlign: "bottom",
    marginRight: "5px",
  };

  const userInfo = userInfoData ? userInfoData : tokenModel && tokenModel.userInfo ? JSON.parse(tokenModel.userInfo) : "";

  const systemLabelItems = [
    { id: "1", name: "管理系统", color: "success" },
    { id: "2", name: "前后端分离", color: "processing" },
    { id: "3", name: "微服务架构", color: "processing" },
    { id: "4", name: "React", color: "error" },
    { id: "5", name: "Umi", color: "warning" },
    { id: "6", name: "Dva", color: "default" },
    { id: "7", name: "服务治理", color: "success" },
    { id: "8", name: "服务网关", color: "success" },
    { id: "9", name: "配置中心", color: "processing" },
    { id: "10", name: "调佣链分析", color: "warning" },
    { id: "11", name: "数据库版本管理", color: "processing" },
    { id: "12", name: "Ant Design", color: "error" },
    { id: "13", name: "AntV", color: "warning" },
    { id: "14", name: "PostgreSql", color: "default" },
    { id: "15", name: "Redis", color: "success" },
  ];

  return (
    <div id={"userInformation"} className={styles.userInfoShow}>
      <div style={{margin: "0px 0px 10px 0px"}}>
        <i className="ri-file-text-line" style={{fontSize: "18px", verticalAlign: "sub", marginRight: "5px"}}/>个人资料
      </div>
      <div className={styles.cardHeader}>
        <div style={{ textAlign: "center", marginBottom: "5px" }}>
          <Avatar shape="square" size={80} src={userInfo.imgBase64} icon={<i className={"ri-user-line"} />} />
        </div>
        <Divider className={styles.divider} />
        <div>
          <div><i className={"ri-book-open-line"} style={iconStyle}/>&nbsp;个人简介：</div>
          <div className={styles.resumeDiv}>
            来得及佛安慰奖佛山代理费降温哦，辅导机构二极管懒得开房间供热，辅导机构而肌肉IG两地分居供热，快递费机构诶热加工搜狗加热噢平均，大姐夫搜狗建瓯市攻击力东方红郡ower及地方了空间欧派施工记录等方式可将盘goes日。
          </div>
        </div>
        <Divider className={styles.divider} />
        <div>
          <span><i className={"ri-user-line"} style={iconStyle}/>&nbsp;登录名称：</span>
          <span className={styles.personInfoSpan}>{userInfo ? userInfo.userName : ""}</span>
        </div>
        <div>
          <span><i className={"ri-file-user-line"} style={iconStyle}/>&nbsp;用户角色：</span>
          <span className={styles.personInfoSpan}>{userInfo ? userInfo.userName : ""}</span>
        </div>
        <div>
          <span><i className={"ri-cellphone-line"} style={iconStyle}/>&nbsp;手机号码：</span>
          <span className={styles.personInfoSpan}>{userInfo ? userInfo.phoneNumber : ""}</span>
        </div>
        <div>
          <span><i className={"ri-mail-line"} style={iconStyle}/>&nbsp;邮箱地址：</span>
          <span className={styles.personInfoSpan}>{userInfo ? userInfo.email : ""}</span>
        </div>
        <div>
          <span><i className={"ri-time-line"} style={iconStyle}/>&nbsp;创建时间：</span>
          <span className={styles.personInfoSpan}>{userInfo ? userInfo.createTime.substr(0, userInfo.createTime.indexOf("T")) : ""}</span>
        </div>
        <Divider className={styles.divider} />
        <div>
          <div><i className={"ri-file-mark-line"} style={iconStyle}/>&nbsp;个性标签：</div>
          <div className={styles.individualDiv}>
            {
              systemLabelItems.map(item =>
                <Tag color={item.color} style={{ margin: "0px 5px 5px 0px" }}>{item.name}</Tag>)
            }
          </div>
        </div>
      </div>
    </div>
  )
};

export default UserInformation;
