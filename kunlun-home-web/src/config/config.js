const GATEWAY_SERVICE_API = 'http://localhost:8015/kunlun-gateway-service';
const BASEDATA_SERVICE_API = 'http://localhost:8015/kunlun-basedata-service';
const SYSTEM_SERVICE_API = 'http://localhost:8015/kunlun-system-service';
const WEB_SYSTEM_API = 'http://localhost:8005';
const ACTIVITI_PROCESS_API = "http://localhost:8025";
const config = {
  name: '昆仑管理系统',
  footerText: '昆仑管理系统 © 2018-2028 KunLun Admin Copyright | Version 1.0',
  LIMIT_SIZE: 5,
  PAGE_SIZE: 10,
  WEB_SYSTEM_API,
  GATEWAY_SERVICE_API,
  BASEDATA_SERVICE_API,
  SYSTEM_SERVICE_API,
  ACTIVITI_PROCESS_API,
  DEFAULT_THEME_COLOR: "#31ac90",
  DEFAULT_SIDER_COLOR: "#ffffff",
  base_cache_api: {
    // 验证码
    getAuthCode: `${BASEDATA_SERVICE_API}/code/getAuthCode`,

    // 系统菜单
    getAppMenu: `${BASEDATA_SERVICE_API}/menu/getAppMenu`,

    // 消息管理
    getAllMessages: `${SYSTEM_SERVICE_API}/message/getAllMessages`,
  },
  gate_way_api: {
    // 登录
    login: `${GATEWAY_SERVICE_API}/shiro/login`,
    logout: `${GATEWAY_SERVICE_API}/shiro/logout`,
  },
  system_api: {
    // 待办
    getTodoList: `${SYSTEM_SERVICE_API}/processList`,
  },
  frame_menu: {
    main: [
      { key: "home", url: "", icon: "ri-home-4-line", name: "我的地盘", show: true },
      { key: "user", url: "", icon: "ri-user-line", name: "用户管理", show: true },
      { key: "synergy", url: "", icon: "ri-codepen-line", name: "协同管理", show: true },
      { key: "resource", url: "", icon: "ri-file-cloud-line", name: "资源管理", show: true },
      { key: "option", url: "", icon: "ri-settings-3-line", name: "系统管理", show: true },
    ],
    sider: {
      home: [
        { key: "platform", url: WEB_SYSTEM_API + "/home/platform", icon: "ri-device-line", name: "工作空间", show: true },
        { key: "console", url: WEB_SYSTEM_API + "/home/console", icon: "ri-airplay-line", name: "监控平台", show: true },
        { key: "person", url: WEB_SYSTEM_API + "/home/person", icon: "ri-user-settings-line", name: "个人信息", show: true }
      ],
      user: [
        { key: "company", url: WEB_SYSTEM_API + "/user/company", icon: "ri-database-line", name: "单位管理", show: true },
        { key: "department", url: WEB_SYSTEM_API + "/user/department", icon: "ri-device-line", name: "部门管理", show: true },
        { key: "post", url: WEB_SYSTEM_API + "/user/post", icon: "ri-user-settings-line", name: "岗位管理", show: true },
        { key: "list", url: WEB_SYSTEM_API + "/user/list", icon: "ri-team-line", name: "人员用户", show: true },
        { key: "correlate", url: WEB_SYSTEM_API + "/user/correlate", icon: "ri-file-user-line", name: "关联管理", show: true },
        { key: "authorize", url: WEB_SYSTEM_API + "/user/authorize", icon: "ri-stack-line", name: "授权管理", show: true },
        { key: "online", url: WEB_SYSTEM_API + "/user/online", icon: "ri-global-line", name: "在线用户", show: true },
        { key: "amap", url: WEB_SYSTEM_API + "/user/amap", icon: "ri-map-pin-user-line", name: "用户地图", show: true },
      ],
      synergy: [
        { key: "model", url: WEB_SYSTEM_API + "/synergy/model", icon: "ri-government-line", name: "模型管理", show: true },
        { key: "create", url: ACTIVITI_PROCESS_API + "/service/create", icon: "ri-community-line", name: "模型创建", show: false },
        { key: "update", url: ACTIVITI_PROCESS_API + "/static/modeler.html?modelId=", icon: "ri-hotel-line", name: "模型编辑", show: false },
        { key: "process", url: WEB_SYSTEM_API + "/synergy/process", icon: "ri-qr-scan-line", name: "流程管理", show: true },
        { key: "todo", url: WEB_SYSTEM_API + "/synergy/todo", icon: "ri-todo-line", name: "待办任务", show: false },
        { key: "log", url: WEB_SYSTEM_API + "/synergy/log", icon: "ri-article-line", name: "操作日志", show: true },
        { key: "schedule", url: WEB_SYSTEM_API + "/synergy/schedule", icon: "ri-calendar-todo-line", name: "事项日程", show: true },
      ],
      resource: [
        { key: "link", url: WEB_SYSTEM_API + "/resource/link", icon: "ri-links-line", name: "外链管理", show: true },
        { key: "monitor", url: WEB_SYSTEM_API + "/resource/monitor", icon: "ri-dashboard-line", name: "监控统计", show: true },
        { key: "virtual", url: WEB_SYSTEM_API + "/resource/virtual", icon: "ri-eye-line", name: "环境监控", show: true },
        { key: "picture", url: WEB_SYSTEM_API + "/resource/picture", icon: "ri-image-fill", name: "图片管理", show: true },
      ],
      option: [
        { key: "menu", url: WEB_SYSTEM_API + "/option/menu", icon: "ri-windows-line", name: "菜单管理", show: true },
        { key: "icon", url: WEB_SYSTEM_API + "/option/icon", icon: "ri-remixicon-line", name: "图标管理", show: true },
        { key: "dictionary", url: WEB_SYSTEM_API + "/option/dictionary", icon: "ri-book-read-line", name: "数据字典", show: true },
        { key: "navigate", url: WEB_SYSTEM_API + "/option/navigate", icon: "ri-drag-move-line", name: "导航管理", show: true },
        { key: "notice", url: WEB_SYSTEM_API + "/option/notice", icon: "ri-notification-line", name: "通知公告", show: true },
        { key: "info", url: WEB_SYSTEM_API + "/option/info", icon: "ri-information-line", name: "关于我们", show: true },
      ]
    }
  },
  theme_color: [
    {key: "1", value: "#5adf96", name: "叶兰绿"},
    {key: "2", value: "#f5515f", name: "赤诚红"},
    {key: "3", value: "#9958dc", name: "玉烟紫"},
    {key: "4", value: "#f7889c", name: "芙蕖粉"},
    {key: "5", value: "#304269", name: "露莓黑"},
    {key: "6", value: "#1890ff", name: "经典蓝"},
  ]
};

export default config;
