import React from 'react';
import { connect } from 'umi';
import { Layout } from 'antd';
import Login from '../components/login/Login';

const { Content } = Layout;

/**
 * 登录界面
 */
const LoginPage = (props) => {

  const { dispatch, globalModel } = props;
  const { codeModel, pageLoading } = globalModel;

  const loginProps = {
    codeModel,
    pageLoading,
    onLogin: (data) => {
      dispatch({ type: "globalModel/login", payload: data });
    },
    refreshCode: () => {
      dispatch({ type: "globalModel/getAuthCode", payload: {}});
    },
  };

  return (
    <Layout style={{ width: window.innerWidth, height: window.innerHeight, background: "#31ac90" }}>
      <Content style={{ height: "100%" }}>
        <Login {...loginProps} />
      </Content>
    </Layout>
  )
}

function mapStateToProps({ globalModel }) {
  return { globalModel };
}

export default connect(mapStateToProps)(LoginPage);
