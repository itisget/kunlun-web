const config = {
  DISTRICt_LABEL: [{
    adcode: 410000,
    name: "河南",
    position: [113.665, 34.757],
    opacity: 0.5,
    zIndex: 10,
    text: {content: "河南", direction: "center", style: {fontSize: 12, fillColor: "#0a0a0a"}}
  }, {
    adcode: 650000,
    name: "新疆",
    position: [87.617, 43.792],
    opacity: 0.5,
    zIndex: 10,
    text: {content: "新疆", direction: "center", style: {fontSize: 12, fillColor: "#0a0a0a"}}
  }, {
    adcode: 420000,
    name: "湖北",
    position: [114.298, 30.584],
    opacity: 0.5,
    zIndex: 10,
    text: {content: "湖北", direction: "center", style: {fontSize: 12, fillColor: "#0a0a0a"}}
  }, {
    adcode: 610000,
    name: "陕西",
    position: [108.948, 34.263],
    opacity: 0.5,
    zIndex: 10,
    text: {content: "陕西", direction: "center", style: {fontSize: 12, fillColor: "#0a0a0a"}}
  }, {
    adcode: 310000,
    name: "上海",
    position: [121.472, 31.231],
    opacity: 0.5,
    zIndex: 10,
    text: {content: "上海", direction: "center", style: {fontSize: 12, fillColor: "#0a0a0a"}}
  }, {
    adcode: 500000,
    name: "重庆",
    position: [106.504, 29.533],
    opacity: 0.5,
    zIndex: 10,
    text: {content: "重庆", direction: "center", style: {fontSize: 12, fillColor: "#0a0a0a"}}
  }, {
    adcode: 540000,
    name: "西藏",
    position: [91.132, 29.66],
    opacity: 0.5,
    zIndex: 10,
    text: {content: "西藏", direction: "center", style: {fontSize: 12, fillColor: "#0a0a0a"}}
  }, {
    adcode: 340000,
    name: "安徽",
    position: [117.283, 31.861],
    opacity: 0.5,
    zIndex: 10,
    text: {content: "安徽", direction: "center", style: {fontSize: 12, fillColor: "#0a0a0a"}}
  }, {
    adcode: 460000,
    name: "海南",
    position: [110.331, 20.031],
    opacity: 0.5,
    zIndex: 10,
    text: {content: "海南", direction: "center", style: {fontSize: 12, fillColor: "#0a0a0a"}}
  }, {
    adcode: 630000,
    name: "青海",
    position: [101.778, 36.623],
    opacity: 0.5,
    zIndex: 10,
    text: {content: "青海", direction: "center", style: {fontSize: 12, fillColor: "#0a0a0a"}}
  }, {
    adcode: 450000,
    name: "广西",
    position: [108.32, 22.824],
    opacity: 0.5,
    zIndex: 10,
    text: {content: "广西", direction: "center", style: {fontSize: 12, fillColor: "#0a0a0a"}}
  }, {
    adcode: 640000,
    name: "宁夏",
    position: [106.278, 38.466],
    opacity: 0.5,
    zIndex: 10,
    text: {content: "宁夏", direction: "center", style: {fontSize: 12, fillColor: "#0a0a0a"}}
  }, {
    adcode: 360000,
    name: "江西",
    position: [115.892, 28.676],
    opacity: 0.5,
    zIndex: 10,
    text: {content: "江西", direction: "center", style: {fontSize: 12, fillColor: "#0a0a0a"}}
  }, {
    adcode: 810000,
    name: "香港",
    position: [114.173, 22.32],
    opacity: 0.5,
    zIndex: 10,
    text: {content: "香港", direction: "center", style: {fontSize: 12, fillColor: "#0a0a0a"}}
  }, {
    adcode: 150000,
    name: "内蒙古",
    position: [111.67, 40.818],
    opacity: 0.5,
    zIndex: 10,
    text: {content: "内蒙古", direction: "center", style: {fontSize: 12, fillColor: "#0a0a0a"}}
  }, {
    adcode: 620000,
    name: "甘肃",
    position: [103.823, 36.058],
    opacity: 0.5,
    zIndex: 10,
    text: {content: "甘肃", direction: "center", style: {fontSize: 12, fillColor: "#0a0a0a"}}
  }, {
    adcode: 510000,
    name: "四川",
    position: [104.065, 30.659],
    opacity: 0.5,
    zIndex: 10,
    text: {content: "四川", direction: "center", style: {fontSize: 12, fillColor: "#0a0a0a"}}
  }, {
    adcode: 120000,
    name: "天津",
    position: [117.19, 39.125],
    opacity: 0.5,
    zIndex: 10,
    text: {content: "天津", direction: "center", style: {fontSize: 12, fillColor: "#0a0a0a"}}
  }, {
    adcode: 140000,
    name: "山西",
    position: [112.549, 37.857],
    opacity: 0.5,
    zIndex: 10,
    text: {content: "山西", direction: "center", style: {fontSize: 12, fillColor: "#0a0a0a"}}
  }, {
    adcode: 530000,
    name: "云南",
    position: [102.712, 25.04],
    opacity: 0.5,
    zIndex: 10,
    text: {content: "云南", direction: "center", style: {fontSize: 12, fillColor: "#0a0a0a"}}
  }, {
    adcode: 220000,
    name: "吉林",
    position: [125.324, 43.886],
    opacity: 0.5,
    zIndex: 10,
    text: {content: "吉林", direction: "center", style: {fontSize: 12, fillColor: "#0a0a0a"}}
  }, {
    adcode: 710000,
    name: "台湾",
    position: [121.509, 25.044],
    opacity: 0.5,
    zIndex: 10,
    text: {content: "台湾", direction: "center", style: {fontSize: 12, fillColor: "#0a0a0a"}}
  }, {
    adcode: 820000,
    name: "澳门",
    position: [113.549, 22.198],
    opacity: 0.5,
    zIndex: 10,
    text: {content: "澳门", direction: "center", style: {fontSize: 12, fillColor: "#0a0a0a"}}
  }, {
    adcode: 520000,
    name: "贵州",
    position: [106.713, 26.578],
    opacity: 0.5,
    zIndex: 10,
    text: {content: "贵州", direction: "center", style: {fontSize: 12, fillColor: "#0a0a0a"}}
  }, {
    adcode: 430000,
    name: "湖南",
    position: [112.982, 28.194],
    opacity: 0.5,
    zIndex: 10,
    text: {content: "湖南", direction: "center", style: {fontSize: 12, fillColor: "#0a0a0a"}}
  }, {
    adcode: 210000,
    name: "辽宁",
    position: [123.429, 41.796],
    opacity: 0.5,
    zIndex: 10,
    text: {content: "辽宁", direction: "center", style: {fontSize: 12, fillColor: "#0a0a0a"}}
  }, {
    adcode: 130000,
    name: "河北",
    position: [114.502, 38.045],
    opacity: 0.5,
    zIndex: 10,
    text: {content: "河北", direction: "center", style: {fontSize: 12, fillColor: "#0a0a0a"}}
  }, {
    adcode: 370000,
    name: "山东",
    position: [117, 36.675],
    opacity: 0.5,
    zIndex: 10,
    text: {content: "山东", direction: "center", style: {fontSize: 12, fillColor: "#0a0a0a"}}
  }, {
    adcode: 320000,
    name: "江苏",
    position: [118.767, 32.041],
    opacity: 0.5,
    zIndex: 10,
    text: {content: "江苏", direction: "center", style: {fontSize: 12, fillColor: "#0a0a0a"}}
  }, {
    adcode: 330000,
    name: "浙江",
    position: [120.153, 30.287],
    opacity: 0.5,
    zIndex: 10,
    text: {content: "浙江", direction: "center", style: {fontSize: 12, fillColor: "#0a0a0a"}}
  }, {
    adcode: 350000,
    name: "福建",
    position: [119.306, 26.075],
    opacity: 0.5,
    zIndex: 10,
    text: {content: "福建", direction: "center", style: {fontSize: 12, fillColor: "#0a0a0a"}}
  }, {
    adcode: 440000,
    name: "广东",
    position: [113.28, 23.125],
    opacity: 0.5,
    zIndex: 10,
    text: {content: "广东", direction: "center", style: {fontSize: 12, fillColor: "#0a0a0a"}}
  }, {
    adcode: 230000,
    name: "黑龙江",
    position: [126.642, 45.756],
    opacity: 0.5,
    zIndex: 10,
    text: {content: "黑龙江", direction: "center", style: {fontSize: 12, fillColor: "#0a0a0a"}}
  }],

  GDP_SPEED: {
    '520000': 10,//贵州
    '540000': 10,//西藏
    '530000': 8.5,//云南
    '500000': 8.5,//重庆
    '360000': 8.5,//江西
    '340000': 8.0,//安徽
    '510000': 7.5,//四川
    '350000': 8.5,//福建
    '430000': 8.0,//湖南
    '420000': 7.5, //湖北
    '410000': 7.5,//河南
    '330000': 7.0,//浙江
    '640000': 7.5,//宁夏
    '650000': 7.0,//新疆
    '440000': 7.0,//广东
    '370000': 7.0,//山东
    '450000': 7.3,//广西
    '630000': 7.0,//青海
    '320000': 7.0,//江苏
    '140000': 6.5,//山西
    '460000': 7,// 海南
    '310000': 6.5,//上海
    '110000': 6.5, // 北京
    '130000': 6.5, // 河北
    '230000': 6, // 黑龙江
    '220000': 6,// 吉林
    '210000': 6.5, //辽宁
    '150000': 6.5,//内蒙古
    '120000': 5,// 天津
    '620000': 6,// 甘肃
    '610000': 8.5,// 甘肃
    '710000': 2.64, //台湾
    '810000': 3.0, //香港
    '820000': 4.7 //澳门
  }
};

export default config;
