import * as monitorService from '../../services/resource/monitorService';

export default {
  namespace: "monitorModel",
  state: {
    machineList: []
  },
  reducers: {
    updateState(state, { payload }) {
      return { ...state, ...payload }
    },
  },
  effects: {
    *getSystemMonitorData({ payload: params }, { select, put, call }) {
      const res = yield call(monitorService.getSystemMonitorData, params);
      if (res.code == "200") {
        yield put({ type: "updateState", payload: { machineList: res.data }});
      }
    }
  },
  subscriptions: {
    setup({ dispatch, history }) {
      const pathname = window.location.pathname;
      console.log("monitorModel subscriptions setup pathname => ", pathname);
      if (pathname === "/resource/monitor") {
        dispatch({ type: 'getSystemMonitorData', payload: { pageSize: 100 }});
      }
    },
  },
};
