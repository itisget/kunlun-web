import * as linkService from '../../services/resource/linksService';
import { message } from 'antd';
import config from '../../config/config';

export default {
  namespace: "linksModel",
  state: {
    systemLinkLoading: false,
    customLinkLoading: false,
    systemLinkList: null,
    customLinkList: null,
    linkModalVisible: false,
    operateType: "add",
    linkInfoData: null,
    saveLoading: false,
    editVisible: false,
    currentLinkName: null,
    linksModalVisible: false,
    linkRecord: null,
    linkType: "system",
  },
  reducers: {
    updateState(state, { payload }) {
      return { ...state, ...payload }
    },
  },
  effects: {
    *getLinksList({ payload: params }, { select, call, put }) {
      const { dataType } = params;
      const payloadTrue = "system" == dataType ? { systemLinkLoading: true } : "custom" == dataType ? { customLinkLoading: true } : { systemLinkLoading: true, customLinkLoading: true };
      yield put({ type: "updateState", payload: payloadTrue });
      const res = yield call(linkService.getLinksList, { ...params });
      if (res.code == "200") {
        let data = res.data, payload = {};
        if (!dataType) {
          payload = { systemLinkList: data.system, customLinkList: data.custom };
        } else if ("system" == dataType) {
          payload = { systemLinkList: data.system };
        } else if ("custom" == dataType) {
          payload = { customLinkList: data.custom };
        }
        yield put({ type: "updateState", payload });
      }
      const payloadFalse = "system" == dataType ? { systemLinkLoading: false } : "custom" == dataType ? { customLinkLoading: false } : { systemLinkLoading: false, customLinkLoading: false };
      yield put({ type: "updateState", payload: payloadFalse });
    },

    *onSave({ payload: params }, { select, call, put }) {
      const res = yield call(linkService.onSave, params);
      if (res.code == "200") {
        yield put({ type: "getLinksList", payload: { dataType: params.type }});
        yield put({ type: "updateState", payload: { linksModalVisible: false }});
        message.info("添加成功！");
      } else {
        message.info("添加失败！");
      }
    },

    *onDelete({ payload: params }, { select, call, put }) {
      // const res = yield call(menuService.deleteMenu, params);
      // if (res.code == "200") {
      //   yield put({ type: "getMenuTreeList", payload: {}});
      //   console.log("删除成功！");
      // } else {
      //   console.log("删除失败！");
      // }
    },

    *onRefresh({ payload: params }, { select, call, put }) {
      const res = yield call(linkService.refreshMonitor, params);
      if (res.code == "200") {
        yield put({ type: "getMachineList", payload: {}});
        console.log("刷新成功！");
      } else {
        console.log("刷新失败！");
      }
    }
  },
  subscriptions: {
    setup({ dispatch, history }) {
      const pathname = window.location.pathname;
      console.log("linksModel subscriptions setup pathname => ", pathname);
      if (location.pathname === "/resource/link") {
        dispatch({ type: 'getLinksList', payload: {} });
      }
    },
  },
};
