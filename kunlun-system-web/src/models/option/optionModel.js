export default {
  namespace: 'optionModel',
  state: {
    modalVisible: false,
  },
  reducers: {
    updateState(state, { payload }) {
      return { ...state, ...payload }
    },
  },
  effects: {
    *updateStateDatas({ payload }, { call, put }) {
      yield put({ type: 'updateState', payload });
    },
  },
  subscriptions: {
    setup({ dispatch, history }) {
      const pathname = window.location.pathname;
      console.log("optionModel subscriptions setup pathname => ", pathname);
    },
  },
};
