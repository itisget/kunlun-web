import * as navigationService from '../../services/option/navigationService';
import { message } from "antd";

export default {
  namespace: "navigationModel",
  state: {
    navigationLoading: false,
    navigationList: [],
    operateType: 'add',
    navigationInfoData: null,
    searchParams: null,
    navigateModalVisible: false,
  },

  reducers: {
    updateState(state, { payload }) {
      return { ...state, ...payload }
    },
  },

  effects: {
    *getListDatas({payload: {params}}, { select, call, put }) {
      yield put({ type: "updateState", payload: { navigationLoading: true }});
      const res = yield call(navigationService.getAllNavigate, { ...params });
      if (res.code == "200") {
        yield put({ type: 'updateState', payload: { navigationList: res.data }});
      }
      yield put({ type: "updateState", payload: { navigationLoading: false }});
    },

    *saveNavigation({payload: params}, {select, call, put}) {
      const res = yield call(navigationService.addNavigate, params);
      if (res.code == 200 || res.code == 201) {
        message.info("新增成功！");
        yield put({ type: 'getListDatas', payload: {}});
        yield put({ type: "updateState", payload: { navigationModalVisible: false }});
      } else {
        message.info("新增失败！");
      }
    },

    *updateNavigation({payload: params}, {select, call, put}) {
      const res = yield call(navigationService.updateNavigate, params);
      if (res.code == 200) {
        message.info("编辑成功！");
        yield put({ type: 'getListDatas', payload: {}});
      } else {
        message.info("编辑失败！");
      }
    },
  },

  subscriptions: {
    setup({ dispatch, history }) {
      const pathname = window.location.pathname;
      console.log("navigationModel subscriptions setup pathname => ", pathname);
      if (pathname === "/option/navigate") {
        dispatch({ type: 'getListDatas', payload: {}});
      }
    },
  },
};
