import * as departmentService from '../../services/user/departmentService';
import * as workPostService from '../../services/user/workPostService';
import * as roleService from '../../services/user/roleService';
import * as allotAuthorizeService from '../../services/user/allotAuthorizeService';
import * as userService from '../../services/user/userService';
import { message } from "antd";
import config from '../../config/config';

/**
 * 部门管理Model
 */
export default {
  namespace: "allotAuthorizeModel",
  state: {
    listLoading: false,
    authorizeData: [],
    total: 0,
    currentPage: 0,
    pageSize: 0,
    departmentModalVisible: false,
    operateType: 'add',
    departmentInfoData: null,
    selectedRowKeys: [],
    searchParams: null,
    correlateList: [],
    radioValue: "department",
    menuLimitDrawerVisible: false,
    userAllotTransferVisible: false,
    viewAuthorizeDrawerVisible: false,
    allotId: null,
    allUserList: [],
    menuList: [],
    correlateRecord: null,
    correlateSelectedKeys: null,
    authorizeDetailRecord: null,
    menuDrawerType: "authorize",
  },

  reducers: {
    updateState(state, { payload }) {
      return { ...state, ...payload }
    },
  },

  effects: {
    *init({payload: params}, {select, call, put}) {
      const {radioValue} = yield select(state => state.correlateAuthorizeModel);
      yield put({ type: 'getListDatas', payload: {radioValue}});
    },

    *getListDatas({payload: params}, { select, call, put }) {
      yield put({ type: "updateState", payload: { listLoading: true }});
      const {itemName, radioValue} = params;
      let res = [];
      if ("department" == radioValue) {
        res = yield call(departmentService.getAllDepartment, { currentPage: 1, pageSize: 999999 });
      } else if ("post" == radioValue) {
        res = yield call(workPostService.getAllWorkPost, { currentPage: 1, pageSize: 999999 });
      } else {
        res = yield call(roleService.getAllRole, { currentPage: 1, pageSize: 999999 });
      }
      if (res.code == "200") {
        let correlateList = res.data.records;
        let {correlateRecord, correlateSelectedKeys} = allotAuthorizeService.resolveAllotRecord(correlateList, radioValue);
        yield put({ type: 'getAllotAuthorizeData', payload: { allotId: correlateRecord && correlateRecord.id, radioValue }});
        yield put({ type: 'updateState', payload: { correlateList, correlateRecord, correlateSelectedKeys }});
      }
      yield put({ type: "updateState", payload: { listLoading: false }});
    },

    *getAllotAuthorizeData({payload: params}, { select, call, put }) {
      yield put({ type: "updateState", payload: { listLoading: true }});
      const {itemName, radioValue} = params;
      let res = [];
      if ("department" == radioValue) {
        res = yield call(allotAuthorizeService.getAllotDepartments, { ...params, currentPage: 1, pageSize: 999999 });
      } else if ("post" == radioValue) {
        res = yield call(allotAuthorizeService.getAllotPosts, { ...params, currentPage: 1, pageSize: 999999 });
      } else {
        res = yield call(allotAuthorizeService.getAllotRoles, { ...params, currentPage: 1, pageSize: 999999 });
      }
      if (res.code == "200") {
        const {total, currentPage, pageSize, records} = res.data;
        yield put({ type: 'updateState', payload: { authorizeData: records, total, currentPage, pageSize }});
      }
      yield put({ type: "updateState", payload: { listLoading: false }});
    },

    *getAllUserList({payload: {currentPage = 1, pageSize = config.PAGE_SIZE, params}}, { select, call, put }) {
      yield put({ type: "updateState", payload: { listLoading: true }});
      const res = yield call(userService.getAllUser, { ...params, currentPage, pageSize });
      if (res.code == "200") {
        yield put({ type: 'updateState', payload: { allUserList: res.data.records }});
      }
      yield put({ type: "updateState", payload: { listLoading: false }});
    },

    *onAllotCorrelate({payload: params}, { select, call, put }) {
      yield put({ type: "updateState", payload: { listLoading: true }});
      const res = yield call(allotAuthorizeService.onAllotCorrelate, { ...params });
      if (res.code == "200") {
        const {radioValue} = yield select(state => state.correlateAuthorizeModel);
        yield put({ type: 'getListDatas', payload: { radioValue }});
      }
    },

    *getAllotAuthorizeDetail({payload: params}, { select, call, put }) {
      const res = yield call(allotAuthorizeService.getAllotAuthorizeDetail, { ...params });
      if (res.code == "200") {
        yield put({ type: 'updateState', payload: { authorizeDetailRecord: res.data }});
      }
    },

    *deleteAllots({payload: params}, { select, call, put }) {
      const res = yield call(allotAuthorizeService.deleteAllots, { ...params });
      if (res.code == "200") {
        message.success("删除成功");
      } else {
        message.success("删除失败");
      }
    },

    *getMenuList({payload: params}, {select, put, call}) {
      const res = yield call(roleService.getMenuList, {...params, currentPage: 1, pageSize: 999999});
      yield put({type: "updateState", payload: {menuList: res.data.records}});
    },

    *updateDepartment({payload: params}, {put, call}) {
      const res = yield call(departmentService.updateDepartment, params);
      if (res.code == "200") {
        message.info("修改成功！");
        yield put({ type: 'updateState', payload: { departmentModalVisible: false }});
        yield put({ type: 'getListDatas', payload: {}});
      } else {
        message.info("修改失败！");
      }
    },
  },

  subscriptions: {
    setup({ dispatch, history }) {
      const pathname = window.location.pathname;
      console.log("allotAuthorizeModel subscriptions setup pathname => ", pathname);
      if (pathname === "/user/authorize") {
        dispatch({ type: 'init', payload: {}});
      }
    },
  },
}
