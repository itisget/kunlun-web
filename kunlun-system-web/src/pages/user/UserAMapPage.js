import React from 'react';
import { connect } from 'umi';
import { Map, APILoader } from '@uiw/react-amap';
import config from '../../config/config';
import amapConfig from '../../config/amapConfig';
import styles from './../index.css';
import AMapTools from '../../components/user/amap/AMapTools';

const UserAMapPage = (props) => {

  const {dispatch, location, amapModel} = props;
  const {zoom, center} = amapModel;

  const colors = {};
  const LabelsData = amapConfig.DISTRICt_LABEL;
  const GDPSpeed = amapConfig.GDP_SPEED;
  const getColorByDGP = (adcode) => {
    if (!colors[adcode]) {
      const gdp = GDPSpeed[adcode];
      if (!gdp) {
        colors[adcode] = 'rgb(227, 227, 227)';
      } else {
        // 统一颜色算法
        var r = 3;
        var g = 140;
        var b = 230;
        var a = gdp/10;
        colors[adcode] = 'rgba(' + r + ',' + g + ',' + b + ',' + a + ')';

        // 不同颜色算法
        // var rg = 255 - Math.floor((gdp - 5) / 5 * 255);
        // colors[adcode] = 'rgb(' + rg + ',' + rg + ',255)';
      }
    }
    return colors[adcode];
  }

  const handleRenderLayer = (AMap, map) => {
    // 简易行政区
    const layerStyles = {
      "stroke-width": 2,
      'nation-stroke': '#ff0000',
      'coastline-stroke': [0.85, 0.63, 0.94, 1],
      'province-stroke': 'white',
      'city-stroke': 'rgba(255,255,255,0.15)',
    };
    const distCountry = new AMap.DistrictLayer.Country({
      zIndex: 10,
      SOC: 'CHN',
      depth: 1,
      styles: {
        ...layerStyles,
        'fill': (props) => {
          console.log("UserAMapPage layerStyles props => ", props);
          return getColorByDGP(props.adcode);
        }
      }
    });

    // 省级标注
    map.on('complete', function() {
      var layer = new AMap.LabelsLayer();
      for (var i = 0; i < LabelsData.length; i++) {
        var labelsMarker = new AMap.LabelMarker(LabelsData[i]);
        layer.add(labelsMarker);
      }
      map.add(layer);
    });

    // 行政区拾取
    var colorFill = 'rgba(49,245,6,0.3)';
    map.on('click', function (ev) {
      var px = ev.pixel;
      // 拾取所在位置的行政区
      var props = distCountry.getDistrictByContainerPos(px);
      if (props) {
        var adcode = props.adcode;
        if (adcode) {
          // 重置行政区样式
          distCountry.setStyles({
            ...layerStyles,
            'fill': function (props) {
              return props.adcode == adcode ? colorFill : getColorByDGP(props.adcode);
            }
          });
          document.getElementById('name').innerText = props.NAME_CHN;
          document.getElementById('eng-name').innerText = props.NAME_ENG;
        }
      }
    });
    map.add(distCountry);
    return;
  };

  return (
    <div id={"appMap"} style={{ width: 'calc(100% - 30px)', height: 'calc(100% - 30px)', padding: "15px" }}>
      <div className={styles.amap_info}>
        <h4>省/市行政区</h4>
        <p>省/市：<span id="name">--</span></p>
        <p>用户数：<span id="eng-name">--</span></p>
        <hr />
        <p>点击地图任意省/市行政区域</p>
      </div>
      <APILoader version="2.0.5" akey={config.amap_info.amapkey}>
        <Map
          style={{ height: "calc(100% - 0px)" }}
          center={center}
          zoom={zoom}
          zoomEnable={false}
          dragEnable={false}
          keyboardEnable={false}
        >
        { ({ AMap, map, container }) => handleRenderLayer(AMap, map) }
        </Map>
      </APILoader>
    </div>
  );
}

function mapStateToProps({ globalModel, amapModel }) {
  return { globalModel, amapModel };
}

export default connect(mapStateToProps)(UserAMapPage);
