import React, {Component} from 'react';
import { connect } from 'umi';
import {Spin} from 'antd';
import NavigationCard from '../../components/home/platform/NavigationCard';
import NewsMessageCard from '../../components/home/platform/NewsMessageCard';
import TodoAuditCard from '../../components/home/platform/TodoAuditCard';
import UserPlatformCard from '../../components/home/platform/UserPlatformCard';
import CalendarScheduleCard from '../../components/home/platform/CalendarScheduleCard';
import UserEstimateCard from '../../components/home/platform/UserEstimateCard';
import UnitStatisticsCard from '../../components/home/platform/UnitStatisticsCard';
import WorkLinkCard from '../../components/home/platform/WorkLinkCard';
import config from "../../config/config";
import indexStyles from './Home.less';
import * as commonUtil from '../../utils/commonUtil';
import moment from 'moment';
import styles from "../../components/home/Home.less";

class PlatformPage extends React.Component {

  constructor(props) {
    super(props);
  }

  componentDidMount() {
    const { dispatch, platformModel } = this.props;
    setInterval(() => {
      const year = moment(new Date()).format("YYYY");
      dispatch({type: "platformModel/getUserCount", payload: {}});
      dispatch({type: "platformModel/onSelectYear", payload: {year}});
      dispatch({type: "platformModel/getRedisInfos", payload: {}});
      dispatch({type: "platformModel/getMessages", payload: {}});
      dispatch({type: "platformModel/queryServiceInvokes", payload: {}});
    }, 1000 * 60);
  }

  componentWillUnmount() {
    clearInterval();
  }

  render() {

    const { dispatch, platformModel } = this.props;

    const {
      userCounts, loading, scheduleData, scheduleIndex, scheduleTotal, scheduleList
    } = platformModel;

    const userInfoCardProps = {
      userCounts,
      onShowDetail: (key) => {
        commonUtil.sendRequestToHome(true, key, null);
      }
    }

    const functionNavigationProps = {
      onShowDetail: (key) => {
        commonUtil.sendRequestToHome(true, key, null);
      }
    }

    const newsMessageChartProps = {
      onShowDetail: (key) => {
        commonUtil.sendRequestToHome(true, key, null);
      },
    }

    const todoAuditCardProps = {
      onShowDetail: (key) => {
        commonUtil.sendRequestToHome(true, key, null);
      },
    }

    const unitStatisticsCardProps = {
      onShowDetail: (key) => {
        commonUtil.sendRequestToHome(true, key, null);
      },
    }

    const workLinkCardProps = {
      onShowDetail: (key) => {
        commonUtil.sendRequestToHome(true, key, null);
      },
    }

    const calendarScheduleProps = {
      scheduleData,
      scheduleIndex,
      scheduleTotal,
      scheduleList,
      onShowDetail: (key) => {
        commonUtil.sendRequestToHome(true, key, null);
      },
      onClickArrow: (arrowType) => {
        dispatch({type: "platformModel/onClickArrow", payload: {arrowType}});
      },
      onCacheCalenarMode: (value, mode) => {
        dispatch({ type: 'scheduleModel/updateState', payload: { calendarMode: mode }});
      }
    }

    const userEstimateProps = {
      scheduleData,
      scheduleIndex,
      scheduleTotal,
      onShowDetail: (key) => {
        commonUtil.sendRequestToHome(true, key, null);
      },
      onClickArrow: (arrowType) => {
        dispatch({type: "platformModel/onClickArrow", payload: {arrowType}});
      }
    }

    return (
      <div className={indexStyles.home_layout_div}>
        <Spin spinning={loading} size={"large"} tip={"数据加载中。。。。。。"}>
          <div className={indexStyles.home_platform_content}>
            <div className={indexStyles.home_platform_bench_card}>
              {/* 工作台、用户统计 */}
              <UserPlatformCard {...userInfoCardProps} />
            </div>
            <div className={indexStyles.home_platform_nav_new}>
              <div className={indexStyles.home_platform_nav_new_show}>
                <div className={styles.home_platform_nav}>
                  {/* 便捷导航 */}
                  <NavigationCard {...functionNavigationProps} />
                </div>
                <div className={indexStyles.home_platform_new}>
                  <div className={indexStyles.home_platform_new_show}>
                    {/* 新闻通知 */}
                    <NewsMessageCard {...newsMessageChartProps} />
                  </div>
                  <div className={indexStyles.home_platform_todo_show}>
                    {/* 待办审核 */}
                    <TodoAuditCard {...todoAuditCardProps} />
                  </div>
                </div>
                <div className={indexStyles.home_platform_unit_show}>
                  {/* 组织统计 */}
                  <UnitStatisticsCard {...unitStatisticsCardProps} />
                </div>
              </div>
              <div className={indexStyles.home_platform_schedule}>
                <div className={styles.home_platform_schedule_show}>
                  {/* 事项日程 */}
                  <CalendarScheduleCard {...calendarScheduleProps} />
                </div>
                <div className={styles.home_platform_estimate_show}>
                  {/* 用户评价 */}
                  <UserEstimateCard {...userEstimateProps} />
                </div>
              </div>
            </div>
            <div className={indexStyles.home_platform_unit_link_show}>
              {/* 协同外链 */}
              <WorkLinkCard {...workLinkCardProps} />
            </div>
          </div>
        </Spin>
      </div>
    );
  }
}

function mapStateToProps({ globalModel, platformModel }) {
  return { globalModel, platformModel };
}

export default connect(mapStateToProps)(PlatformPage);
