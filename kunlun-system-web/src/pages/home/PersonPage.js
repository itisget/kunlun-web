import React, {Component} from 'react';
import { connect } from 'umi';
import {Spin} from 'antd';
import UserInfoCard from '../../components/home/person/UserInfoCard';
import UserBasicCard from '../../components/home/person/UserBasicCard';
import config from "../../config/config";
import styles from './Home.less';

class PersonPage extends React.Component {

  constructor(props) {
    super(props);
  }

  render() {

    const { dispatch, personModel } = this.props;

    const {
      loading, tokenModel, userInfoData, menuData, roleInfoData, onSaveUserInfo, onCloseUserInfo, cacheUserData
    } = personModel;

    const userInformationProps = {
      tokenModel,
      userInfoData
    }

    const userAuthorizationProps = {
      tokenModel,
      menuData,
      roleInfoData,
      onSaveUserInfo,
      onCloseUserInfo,
      userInfoData,
      cacheUserData
    }

    return (
      <div className={styles.home_layout_div}>
        <Spin spinning={loading} size={"large"} tip={"数据加载中。。。。。。"}>
          <div id={"userInfoPage"} className={styles.person_info_auth}>
            {/* 个人资料 */}
            <UserInfoCard {...userInformationProps} />

            {/* 基本信息 */}
            <UserBasicCard {...userAuthorizationProps} />
          </div>

          {/* Footer脚标 */}
          <div className={styles.home_footer_Div}>
            <div className={styles.footFontDiv}>{config.FOOTER_TEXT}</div>
          </div>
        </Spin>
      </div>
    );
  }
}

function mapStateToProps({ globalModel, personModel }) {
  return { globalModel, personModel };
}

export default connect(mapStateToProps)(PersonPage);
