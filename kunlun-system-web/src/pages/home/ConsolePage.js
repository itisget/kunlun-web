import React, {Component} from 'react';
import { connect } from 'umi';
import {Spin} from 'antd';
import UserStatisticsChart from '../../components/home/console/UserStatisticsChart';
import AmapStatisticsCard from '../../components/home/console/AmapStatisticsCard';
import RedisInfoChart from '../../components/home/console/RedisInfoChart';
import MQBrokerChart from '../../components/home/console/MQBrokerChart';
import VisitStatisticsCard from '../../components/home/console/VisitStatisticsCard';
import ServerResourceChart from '../../components/home/console/ServerResourceChart';
import ServerClusterChart from '../../components/home/console/ServerClusterChart';
import ServiceInvokeList from '../../components/home/console/ServiceInvokeList';
import BrowseChart from '../../components/home/console/BrowseChart';
import OperateLogChart from '../../components/home/console/OperateLogChart';
import config from "../../config/config";
import indexStyles from './Home.less';
import * as commonUtil from '../../utils/commonUtil';
import moment from 'moment';

class ConsolePage extends React.Component {

  constructor(props) {
    super(props);
  }

  componentDidMount() {
    const { dispatch, consoleModel } = this.props;
    setInterval(() => {
      const year = moment(new Date()).format("YYYY");
      dispatch({type: "consoleModel/getUserCount", payload: {}});
      dispatch({type: "consoleModel/onSelectYear", payload: {year}});
      dispatch({type: "consoleModel/getRedisInfos", payload: {}});
      dispatch({type: "consoleModel/getMessages", payload: {}});
      dispatch({type: "consoleModel/queryServiceInvokes", payload: {}});
    }, 1000 * 60);
  }

  componentWillUnmount() {
    clearInterval();
  }

  render() {

    const { dispatch, consoleModel } = this.props;

    const { userCounts, redisInfos, mqInfos, loading, scheduleData, scheduleIndex, scheduleTotal, zoom, center,
      serviceInvokes, userStatistics, selectedYear } = consoleModel;

    const serviceInvokeListProps = {
      serviceInvokes,
      onShowDetail: (key) => {
        commonUtil.sendRequestToHome(true, key, null);
      },
    }

    const userInfoCardProps = {
      userCounts,
      onShowDetail: (key) => {
        commonUtil.sendRequestToHome(true, key, null);
      }
    }

    const userStatisticsChartProps = {
      userStatistics,
      selectedYear,
      onSelectYear: (year) => {
        dispatch({type: "consoleModel/updateState", payload: {selectedYear: year}});
        dispatch({type: "consoleModel/onSelectYear", payload: {year}});
      }
    }

    const amapStatisticsChartProps = {
      zoom, center
    }

    const redisInfoChartProps = {
      redisInfos,
    }

    const mqNumberProps = {
      mqInfos,
      onShowDetail: (key) => {
        commonUtil.sendRequestToHome(true, key, null);
      },
    }

    return (
      <div className={indexStyles.home_layout_div}>
        <Spin spinning={loading} size={"large"} tip={"数据加载中。。。。。。"}>
          {/* 监控内容（分上下） */}
          <div className={indexStyles.home_content_div}>
            <div className={indexStyles.work_bench_card_div}>
              {/* 工作台、用户统计 */}
              <VisitStatisticsCard {...userInfoCardProps} />
            </div>
            {/* 资源监控（分左右） */}
            <div className={indexStyles.resource_statistics_show_div}>
              {/* 监控统计（分上中下） */}
              <div className={indexStyles.resource_show_div}>
                <div className={indexStyles.user_access_chart}>
                  {/* 用户访问 */}
                  <UserStatisticsChart {...userStatisticsChartProps}/>
                </div>
                {/* 监控统计（分左右） */}
                <div className={indexStyles.resource_access_chart}>
                  <div className={indexStyles.user_redis_mq_chart}>
                    <div className={indexStyles.redis_info_chart}>
                      {/* Redis资源 */}
                      <RedisInfoChart {...redisInfoChartProps}/>
                    </div>
                    <div className={indexStyles.mq_broker_chart}>
                      {/* MQ队列统计 */}
                      <MQBrokerChart {...mqNumberProps}/>
                    </div>
                  </div>
                  <div className={indexStyles.user_amap_statistics_chart}>
                    {/* 访问地图 */}
                    <AmapStatisticsCard {...amapStatisticsChartProps} />
                  </div>
                </div>
                <div className={indexStyles.tableCDiv}>
                  {/* 服务调用 */}
                  <ServiceInvokeList {...serviceInvokeListProps} />
                </div>
              </div>
              <div className={indexStyles.resource_statistics_div}>
                <div className={indexStyles.server_resource_chart}>
                  {/* 服务器资源 */}
                  <ServerResourceChart />
                </div>
                <div className={indexStyles.server_cluster_chart}>
                  {/* 服务资源 */}
                  <ServerClusterChart />
                </div>
                <div className={indexStyles.browse_visit_chart}>
                  {/* 浏览器分布 */}
                  <BrowseChart />
                </div>
                <div className={indexStyles.operate_log_chart}>
                  {/* 操作日志 */}
                  <OperateLogChart />
                </div>
              </div>
            </div>
          </div>
          {/* Footer脚标 */}
          <div className={indexStyles.home_footer_Div}>
            <div className={indexStyles.footFontDiv}>{config.FOOTER_TEXT}</div>
          </div>
        </Spin>
      </div>
    );
  }
}

function mapStateToProps({ globalModel, consoleModel }) {
  return { globalModel, consoleModel };
}

export default connect(mapStateToProps)(ConsolePage);
