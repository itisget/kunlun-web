import React from 'react';
import { connect } from 'umi';
import { Spin, message, Modal } from 'antd';
import PictureList from "../../components/resource/picture/PictureList";
import PictureToolBar from '../../components/resource/picture/PictureToolBar';
import ViewModal from '../../components/resource/picture/ViewModal';
import PictureSearch from '../../components/resource/picture/PictureSearch';
import TablePagination from "@/components/common/TablePagination";

const PicturePage = (props) => {

  const { dispatch, globalModel, pictureModel } = props;
  const {
    pictureLoading, pictureList, total, currentPage, pageSize, searchParams,
    selectedRowKeys, selectedRows, viewModalVisible, pictureRecord
  } = pictureModel;

  const pictureSearchProps = {
    onSearch: (values) => {
      dispatch({ type: "pictureModel/updateState", payload: { searchParams: values }});
      dispatch({ type: "pictureModel/getPictureList", payload: { currentPage, pageSize, ...values }});
    },
  }

  const pictureToolBarProps = {
    onUpload: (file) => {
      dispatch({ type: "pictureModel/onUpload", payload: file });
    },
    onDelete: () => {
      if (selectedRowKeys.length == 0) {
        message.warning("请选择需要删除的数据！");
        return;
      }

      Modal.confirm({
        title: "删除",
        content: "确定删除选中的记录？",
        onOk() {
          const ids = selectedRowKeys.join(",");
          dispatch({type: "pictureModel/onDelete", payload: { ids }});
        },
        onCancel() {}
      });
    },
  };

  const viewModalProps = {
    viewModalVisible,
    pictureRecord,
    onClose: () => {
      dispatch({ type: 'pictureModel/updateState', payload: { viewModalVisible: false }});
    }
  }

  const pictureListProps = {
    pictureLoading,
    pictureList,
    currentPage,
    pageSize,
    onDownload: (record) => {
      dispatch({ type: "pictureModel/onDownload", payload: record });
    },
    onView: (pictureRecord) => {
      dispatch({ type: 'pictureModel/updateState', payload: { pictureRecord, viewModalVisible: true }});
    },
    rowSelection: {
      selectedRowKeys,
      selectedRows,
      onChange: (selectedRowKeys, selectedRows) => {
        dispatch({ type: 'pictureModel/updateState', payload: { selectedRows, selectedRowKeys }});
      },
    }
  };

  const tablePaginationProps = {
    total,
    currentPage,
    pageSize,
    onPageChange: (currentPage, pageSize) => {
      dispatch({type: 'pictureModel/getPictureList', payload: {currentPage, pageSize, ...searchParams}});
    },
    onShowSizeChange: (currentPage, pageSize) => {
      dispatch({type: 'pictureModel/getPictureList', payload: {currentPage, pageSize, ...searchParams}});
    },
  }

  return (
    <div style={{ padding: "15px" }}>
      <Spin spinning={pictureLoading}>
        <PictureSearch {...pictureSearchProps} />
        <PictureToolBar {...pictureToolBarProps} />
        <ViewModal {...viewModalProps} />
        <PictureList {...pictureListProps} />
        <TablePagination {...tablePaginationProps} />
      </Spin>
    </div>
  );
};

function mapStateToProps({ globalModel, pictureModel }) {
  return { globalModel, pictureModel };
}

export default connect(mapStateToProps)(PicturePage);
