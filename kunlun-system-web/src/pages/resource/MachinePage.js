import React from 'react';
import { connect } from 'umi';
import {Spin} from 'antd';
import MachineList from "../../components/resource/machine/MachineList";
import MachineToolBar from '../../components/resource/machine/MachineToolBar';
import MachineDrawer from '../../components/resource/machine/MachineDrawer';
import MachineSearch from '../../components/resource/machine/MachineSearch';
import TablePagination from "@/components/common/TablePagination";

const MachinePage = (props) => {

  const { dispatch, globalModel, machineModel } = props;
  const { machineLoading, machineList, machineModalVisible, operateType, machineInfoData, saveLoading,
    total, currentPage, pageSize, searchParams, machineInstanceRecord, machineLineData } = machineModel;

  const machineSearchProps = {
    onSearch: (values) => {
      dispatch({ type: "machineModel/getMachineList", payload: values });
    },
  };

  const machineToolBarProps = {
    onRefresh: () => {
      dispatch({ type: "machineModel/onRefresh", payload: { menuModalVisible: true }});
    },
    onExport: () => {
      dispatch({type: "machineModel/downloadTemplate", payload: {}});
    },
  };

  const machineDrawerProps = {
    machineLoading,
    machineModalVisible,
    operateType,
    machineInfoData,
    saveLoading,
    machineInstanceRecord,
    machineLineData,
    onCancel: () => {
      dispatch({ type: "machineModel/updateState", payload: { machineModalVisible: false }});
    },
    onSave: (params) => {
      Promise.all([dispatch({ type: "machineModel/onSave", payload: params })]).then(() =>
        dispatch({ type: "machineModel/updateState", payload: { menuInfoData: null, selectedTreeNode: [], selectedIconRows: [], menuModalVisible: false }}));
    },
    onSelectParentMenu: () => {
      dispatch({ type: "machineModel/updateState", payload: { selectMenuModalVisible: true }});
    },
    onShowIconModal: () => {
      dispatch({ type: "machineModel/updateState", payload: { menuIconModalVisible: true }});
    },
  };

  const machineListProps = {
    currentPage,
    pageSize,
    machineList,
    onDetail: (record) => {
      const { id, serviceName, ipAddress } = record;
      dispatch({ type: "machineModel/getMachineLineData", payload: { id }});
      dispatch({ type: "machineModel/getSystemMonitorData", payload: { serviceName,  ipAddress, pageSize: 50 }});
      dispatch({ type: "machineModel/updateState", payload: { machineModalVisible: true }});
    }
  };

  const tablePaginationProps = {
    total,
    currentPage,
    pageSize,
    onPageChange: (currentPage, pageSize) => {
      dispatch({type: 'machineModel/getMachineList', payload: { currentPage, pageSize, ...searchParams }});
    },
    onShowSizeChange: (currentPage, pageSize) => {
      dispatch({type: 'machineModel/getMachineList', payload: { currentPage, pageSize, ...searchParams }});
    },
  }

  return (
    <div style={{ padding: "15px" }}>
      <Spin spinning={machineLoading}>
        <MachineSearch {...machineSearchProps} />
        <MachineToolBar {...machineToolBarProps} />
        <MachineDrawer {...machineDrawerProps} />
        <MachineList {...machineListProps} />
        <TablePagination {...tablePaginationProps} />
      </Spin>
    </div>
  );
};

function mapStateToProps({ globalModel, machineModel }) {
  return { globalModel, machineModel };
}

export default connect(mapStateToProps)(MachinePage);
