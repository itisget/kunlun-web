import React from 'react';
import { connect } from 'umi';
import LinksList from '../../components/resource/links/LinksList';
import LinksModal from '../../components/resource/links/LinksModal';

const LinksPage = (props) => {

  const { dispatch, globalModel, linksModel } = props;
  const {
    systemLinkLoading, customLinkLoading, systemLinkList, customLinkList, editVisible, currentLinkName,
    linksModalVisible, linkRecord, linkType
  } = linksModel;

  const linksListProps = {
    systemLinkLoading,
    customLinkLoading,
    systemLinkList,
    customLinkList,
    editVisible,
    currentLinkName,
    onShowOperate: (item, value) => {
      dispatch({ type: "linksModel/updateState", payload: { currentLinkName: item.name, editVisible: value }});
    },
    onRefresh: (value) => {
      dispatch({ type: "linksModel/getLinksList", payload: { dataType: value }});
    },
    onAdd: (linkType) => {
      dispatch({ type: "linksModel/updateState", payload: { linksModalVisible: true }});
    },
    onEdit: (linkRecord, linkType) => {
      dispatch({ type: "linksModel/updateState", payload: { linksModalVisible: true, linkRecord, linkType }});
    },
    onDelete: (linkType) => {

    },
    onSearch: (linkName, dataType) => {
      dispatch({ type: "linksModel/getLinksList", payload: { linkName, dataType }});
    },
  }

  const linksModelProps = {
    linksModalVisible,
    linkRecord,
    linkType,
    onSave: (values) => {
      dispatch({ type: "linksModel/onSave", payload: { values }});
    },
    onCancel: () => {
      dispatch({ type: "linksModel/updateState", payload: { linksModalVisible: false }});
    }
  }

  return (
    <div id={"linksPageDiv"} style={{ width: "calc(100% - 30px)", height: "calc(100% - 30px)", padding: "15px" }}>
      <LinksList {...linksListProps} />
      <LinksModal {...linksModelProps} />
    </div>
  );
}

function mapStateToProps({ globalModel, linksModel }) {
  return { globalModel, linksModel };
}

export default connect(mapStateToProps)(LinksPage);
