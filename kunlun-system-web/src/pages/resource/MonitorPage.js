import React from 'react';
import { connect } from 'umi';
import { Card, Row, Col, Spin, Statistic } from 'antd';
import { Line, Column, DualAxes } from '@ant-design/charts';
import moment from 'moment';
import styles from "../../components/home/Home.less";

const MonitorPage = (props) => {

  const { dispatch, monitorModel } = props;
  const { machineList } = monitorModel;

  const dateFormatter = "yyyy-MM-DD HH:mm:ss";
  const netReceiveData = [], netSendData = [];
  for (let i = 0; i < machineList.length; i++) {
    const record = machineList[i];
    record.createTime = moment(record.createTime).format(dateFormatter);
    netReceiveData.push({ createTime: record.createTime, serviceName: record.serviceName + "[每秒接收KB数]", netReceiveKbCount: record.netReceiveKbCount });
    netSendData.push({ createTime: record.createTime, serviceName: record.serviceName + "[每秒发送KB数]", netSendKbCount: record.netSendKbCount });
  }

  const buildPercentLineConfig = (field) => {
    return {
      data: machineList,
      xField: 'createTime',
      yField: field,
      seriesField: 'serviceName',
      color: ['#1979c9', '#D62A0D', '#FAA219', '#27f503'],
      responsive: true,
    };
  }

  const cpuUsedPercentConfig = buildPercentLineConfig("cpuUsedPercent");
  const memoryUsedPercentConfig = buildPercentLineConfig("memoryUsedPercent");
  const diskUsedPercentConfig = buildPercentLineConfig("diskUsedPercent");
  const jvmUsedPercentConfig = buildPercentLineConfig("jvmUsedPercent");

  const netDataConfig = {
    data: [netReceiveData, netSendData],
    xField: 'createTime',
    yField: ['netReceiveKbCount', 'netSendKbCount'],
    geometryOptions: [
      {
        geometry: 'line',
        seriesField: 'serviceName',
        color: ['#1979c9', '#ec0425', '#FAA219', '#27f503'],
      },
      {
        geometry: 'line',
        seriesField: 'serviceName',
        color: ['rgba(152,243,208,0.7)', '#de22f3', '#00eaff', '#ffe066'],
      },
    ],
  }

  return (
    <Spin spinning={false}>
      <Row>
        <Col span={24}>
          <Card title="CPU使用率" bodyStyle={{ padding: "15px" }}>
            <div className={styles.mqCanvas}>
              <Line {...cpuUsedPercentConfig} />
            </div>
          </Card>
        </Col>
      </Row>
      <Row>
        <Col span={24}>
          <Card title="内存使用率" bodyStyle={{ padding: "15px" }}>
            <div className={styles.mqCanvas}>
              <Line {...memoryUsedPercentConfig} />
            </div>
          </Card>
        </Col>
      </Row>
      <Row>
        <Col span={24}>
          <Card title="磁盘使用率" bodyStyle={{ padding: "15px" }}>
            <div className={styles.mqCanvas}>
              <Line {...diskUsedPercentConfig} />
            </div>
          </Card>
        </Col>
      </Row>
      <Row>
        <Col span={24}>
          <Card title="JVM内存使用率" bodyStyle={{ padding: "15px" }}>
            <div className={styles.mqCanvas}>
              <Line {...jvmUsedPercentConfig} />
            </div>
          </Card>
        </Col>
      </Row>
      <Row>
        <Col span={24}>
          <Card title="每秒接收/发送KB数" bodyStyle={{ padding: "15px" }}>
            <div id={"redisMemory"} className={styles.redisCanvas}>
              <DualAxes {...netDataConfig} />
            </div>
          </Card>
        </Col>
      </Row>
    </Spin>
  );
}

function mapStateToProps({ globalModel, monitorModel }) {
  return { globalModel, monitorModel };
}

export default connect(mapStateToProps)(MonitorPage);
