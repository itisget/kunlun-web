import React from 'react';
import { connect } from 'umi';
import NavigationSearch from "../../components/option/navigate/NavigationSearch";
import NavigationToolbar from "../../components/option/navigate/NavigationToolbar";
import NavigationList from "../../components/option/navigate/NavigationList";
import NavigationModal from "../../components/option/navigate/NavigationModal";
import { Modal, message, Spin } from "antd";
import config from "../../config/config";

class NavigationPage extends React.Component {

  render() {

    let {dispatch, globalModel, navigationModel} = this.props;
    const {
      navigationLoading, navigationList, operateType, navigationInfoData,
      searchParams, navigateModalVisible
    } = navigationModel;

    const dictionarySearchProps = {
      onSearch: (searchParams) => {

        debugger

        dispatch({type: "navigationModel/updateState", payload: {searchParams}});
        dispatch({type: 'navigationModel/getListDatas', payload: {...searchParams}});
      },
      onReset: () => {
        dispatch({type: "navigationModel/updateState", payload: {searchParams: null}});
      },
    };

    const dictionaryToolbarProps = {
      addSave: () => {
        dispatch({type: "navigationModel/saveNavigation", payload: { data: JSON.stringify(navigationList) }});
      }
    }

    const dictionaryListProps = {
      navigationLoading,
      navigationList,
      updateList: (values) => {
        for (let i = 0; i < values.length; i++) {
          const record = values[i];
          record.sort = i;
        }
        dispatch({ type: "navigationModel/updateState", payload: { navigationList: values }});
      },
      onEditColor: (record) => {
        dispatch({ type: "navigationModel/updateState", payload: { navigateModalVisible: true }});
      }
    };

    const dictionaryModalProps = {
      navigateModalVisible,
      onCancel: () => {
        dispatch({type: "navigationModel/updateState", payload: {navigateModalVisible: false}});
      },
      onChangeColor: () => {

      },
      onSave: (values) => {
        const type = modalType == "item" ? "navigationModel/saveDictionary" : "navigationModel/saveDictionaryValue";
        dispatch({type, payload: values});
      },
    };

    return (
      <div style={{ padding: "15px" }}>
        <Spin spinning={navigationLoading}>
          <NavigationSearch {...dictionarySearchProps} />
          <NavigationToolbar {...dictionaryToolbarProps} />
          <NavigationList {...dictionaryListProps} />
          <NavigationModal {...dictionaryModalProps} />
        </Spin>
      </div>
    );
  };
}

function mapStateToProps({globalModel, navigationModel}){
  return {globalModel, navigationModel};
}

export default connect(mapStateToProps)(NavigationPage);
