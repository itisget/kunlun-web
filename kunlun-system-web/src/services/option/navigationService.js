import * as request from '../../utils/request';
import config from '../../config/config';

export function getAllNavigate(params) {
  return request.get(`${config.base_api.getAllNavigate}`, params);
}

export function addNavigate(params) {
  return request.post(`${config.base_api.addNavigate}`, { ...params });
}

export function updateNavigate(params) {
  return request.post(`${config.base_api.updateNavigate}`, params);
}
