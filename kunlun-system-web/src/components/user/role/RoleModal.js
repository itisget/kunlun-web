import React from 'react';
import { Modal, Form, Input, Row, Col, Select } from 'antd';
import config from '../../../config/config';
import moment from 'moment';

const FormItem = Form.Item;
const SelectOption = Select.Option;

const RoleModal = (props) => {

  const { roleModalVisible, roleInfoData, operateType, onSave, updateUser, onCancel } = props;

  const [form] = Form.useForm();
  const { getFieldsValue, validateFields, setFieldsValue, resetFields } = form;

  const formItemLayout = {
    labelCol: { span: 5 },
    wrapperCol: { span: 18 },
  };

  const onOk = () => {
    validateFields().then(values => {
      operateType == "add" ? onSave(values) : updateUser(values);
    }).catch(error => {
      console.log("RoleModal Error ===>>> " + error);
    });
  };

  const onClose = () => {
    // resetFields();
    onCancel();
  };

  const statusOptions = config.STATUS_FLAG.map(item => <SelectOption value={item.key}>{item.name}</SelectOption>);

  return (
    <div>
      <Modal
        centered={true}
        visible={roleModalVisible}
        title={operateType == "add" ? "新增" : "修改"}
        onCancel={onClose}
        onOk={onOk}
        width={450}
        maskClosable={false}
        destroyOnClose={false}
      >
        <Form initialValues={roleInfoData} form={form}>
          <Row>
            <Col span={0}>
              <FormItem label="用户ID" name={"id"}>
                <Input />
              </FormItem>
            </Col>
            <Col span={24}>
              <FormItem { ...formItemLayout } label="角色名称" name={"roleName"} rules={[{required: true, message: '请输入角色名称'}]}>
                <Input placeholder={"请输入角色名称"} />
              </FormItem>
            </Col>
            <Col span={24}>
              <FormItem { ...formItemLayout } label="角色字符" name={"roleWord"} rules={[{required: true, message: '请输入角色字符'}]}>
                <Input placeholder={"请输入角色字符"} />
              </FormItem>
            </Col>
            <Col span={24}>
              <FormItem { ...formItemLayout } label="是否启用" name={"status"} rules={[{required: true, message: '请选择是否启用'}]}>
                <Select>
                  { statusOptions }
                </Select>
              </FormItem>
            </Col>
          </Row>
        </Form>
      </Modal>
    </div>
  );
};

export default RoleModal;
