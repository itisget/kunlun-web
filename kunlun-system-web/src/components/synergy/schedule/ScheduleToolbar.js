import React, { Component } from 'react';
import { Button } from 'antd';
import styles from './Schedule.less';

const ScheduleToolbar = (props) => {

  const { addSave, onExport, scheduleList } = props;

  // 未处理日程数
  const outProcessCount = scheduleList.filter(item => moment(item.endTime) < new Date()).length;

  // 待处理日程数
  const notProcessCount = scheduleList.length - outProcessCount;

  const toolBarIconStyle = {};

  const statisticsIconStyle = {
    verticalAlign: "text-top",
    marginRight: "5px",
    fontSize: "18px",
  };

  return (
    <div className={styles.toolBarDiv}>
      <div>
        <Button type="primary" size="default" icon={<i className="ri-add-line" style={toolBarIconStyle}></i>} onClick={() => addSave()}>新建日程</Button>
        <Button type="defalue" size="default" style={{marginLeft: "15px", border: "1px solid #786CE7", color: "#786CE7"}} icon={<i className="ri-download-2-line" style={toolBarIconStyle}></i>} onClick={() => onExport()}>导出</Button>
      </div>
      <div style={{ marginLeft: "15px", background: "#e8e8e8e8", textAlign: "center", padding: "6px 10px 0px 10px" }}>
        <span>
          <i className="ri-time-line" style={{...statisticsIconStyle, color: "green"}} />全部日程
          <span style={{ marginLeft: "4px", color: "green", fontWeight: 800 }}>{scheduleList.length}</span>&nbsp;个
        </span>
        <span style={{ marginLeft: "15px", marginRight: "15px" }}>
          <i className="ri-play-circle-line" style={{...statisticsIconStyle, color: "red"}} />待处理日程
          <span style={{ marginLeft: "4px", color: "red", fontWeight: 800 }}>{outProcessCount}</span>&nbsp;个
        </span>
        <span>
          <i className="ri-forbid-line" style={{...statisticsIconStyle, color: "blue"}} />待处理日程
          <span style={{ marginLeft: "4px", color: "blue", fontWeight: 800 }}>{notProcessCount}</span>&nbsp;个
        </span>
      </div>
    </div>
  );
};

export default ScheduleToolbar;
