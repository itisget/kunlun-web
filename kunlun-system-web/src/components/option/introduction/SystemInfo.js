import React from 'react';
import { Table, Carousel, Descriptions, Divider, Layout, Timeline, Tag } from 'antd';
import styles from './Introduction.less';
import config from '../../../config/config';

const DescriptionsItem = Descriptions.Item;
const { Header, Content, Footer } = Layout;
const TimeLineItem = Timeline.Item;

const SystemInfo = (props) => {

  const iconStyle = {
    verticalAlign: "bottom",
    fontSize: "16px",
  };

  const iconTitleStyle = {
    marginRight: "6px",
    paddingTop: "3px"
  };

  const describeColumns = [
    { title: "名称", dataIndex: "name", key: "name", width: "15%", align: "center" },
    { title: "项目名", dataIndex: "projectName", key: "projectName", width: "25%", align: "center" },
    { title: "说明", dataIndex: "describe", key: "describe", width: "60%" }
  ];
  const descriptionList = [
    { id: "2", name: "前端框架", projectName: "kunlun-home-web", describe: "提供登录页面、业务菜单、消息待办、主题皮肤、登录人信息及项目布局等" },
    { id: "3", name: "前端业务", projectName: "kunlun-system-web", describe: "展示系统业务数据及功能页面，如首页信息、人员管理、用户地图、流程管理、操作日志、事项日程、服务资源管理、菜单管理等" },
    { id: "4", name: "公共模块", projectName: "kunlun-parent-api", describe: "提供SpringBoot、SpringCloud等基础依赖" },
    { id: "5", name: "公共模块", projectName: "kunlun-common-api", describe: "提供公共基础模型、工具、自动配置、统一异常处理、统一Swagger配置及操作日志AOP等等" },
    { id: "6", name: "服务治理", projectName: "kunlun-register-service", describe: "服务注册、服务发现、服务心跳检测、高级消息队列(RabbitMQ)及分布式配置中心等" },
    { id: "7", name: "服务网关", projectName: "kunlun-gateway-service", describe: "服务路由、登录用户校验、鉴权及生成Token、Hystrix的turbine模式配置及Swagger路由配置等" },
    { id: "8", name: "基础数据", projectName: "kunlun-basedata-service", describe: "提供基础数据支持，如菜单、角色、权限等，并提供基于Redis的分布式缓存功能、基于ElasticSearch + RabbitMQ的服务调用追踪、资源爬取等" },
    { id: "9", name: "业务服务", projectName: "kunlun-system-service", describe: "业务功能支持服务，提供业务数据、动态数据源、脚本自动执行及基于RabbitMQ的异步操作日志生成功能" }
  ];

  const timeLineItems = [
    { id: "1", icon: "ri-global-line", color: "green", desc: "V1.2.1 2019-07-02 前台功能逐步完善" },
    { id: "2", icon: "ri-stack-line", color: "blue", desc: "V1.2.0 2019-04-23 集成Activiti流程组件" },
    { id: "3", icon: "ri-stack-line", color: "blue", desc: "V1.1.4 2019-04-15 集成Apache Shiro与Jwt" },
    { id: "4", icon: "ri-stack-line", color: "blue", desc: "V1.1.3 2019-04-05 实现动态数据源切换" },
    { id: "5", icon: "ri-stack-line", color: "blue", desc: "V1.1.2 2019-03-13 SpringBoot整合RabbitMQ" },
    { id: "6", icon: "ri-stack-line", color: "blue", desc: "V1.1.1 2019-01-13 前端与后端整合" },
    { id: "7", icon: "ri-stack-line", color: "blue", desc: "V1.1.0 2018-11-24 搭建SpringCloud工程" },
    { id: "11", icon: "ri-global-line", color: "green", desc: "V1.0.1 2018-10-15 实现菜单切换联动" },
    { id: "13", icon: "ri-global-line", color: "green", desc: "V1.0.0 2018-10-04 搭建单服务前台" }
  ];

  const linkUrlItems = [
    { id: "1", desc: "OCP微服务能力开放平台", className: styles.carsouselOne, url: "http://59.110.164.254:8066/login.html" },
    { id: "2", desc: "PigX快速开发框架", className: styles.carsouselTwo, url: "http://pigx.pig4cloud.com/#/login" },
    { id: "3", desc: "Ant Design Pro", className: styles.carsouselThree, url: "https://preview.pro.ant.design/" },
    { id: "4", desc: "若依管理系统", className: styles.carsouselFour, url: "http://demo.ruoyi.vip/login" },
    { id: "5", desc: "悟空 CRM", className: styles.carsouselFive, url: "http://demo9java.5kcrm.net/" },
    { id: "6", desc: "禅道企业版", className: styles.carsouselSix, url: "http://biz.demo.zentao.net" },
  ];

  const webItems = [
    { id: "1", name: "前端技术栈", url: "https://github.com/facebook/react", desc: "React" },
    { id: "2", name: "前端框架", url: "https://umijs.org/", desc: "Umi" },
    { id: "3", name: "数据流框架", url: "https://dvajs.com/guide/", desc: "Dva" },
    { id: "4", name: "前端UI库", url: "https://ant.design/index-cn", desc: "Ant Design" },
    { id: "5", name: "图表库", url: "https://ant-design-charts-next.antgroup.com/manual/introduction", desc: "Ant Design Charts" },
    { id: "6", name: "图标库", url: "https://remixicon.com/", desc: "Remix Icon" },
    { id: "7", name: "地图组件", url: "https://github.com/ElemeFE/react-amap", desc: "React-amap" },
    { id: "8", name: "富文本编辑器", url: "https://braft.margox.cn/", desc: "Braft-editor" },
    { id: "9", name: "HTTP库", url: "http://www.axios-js.com/", desc: "Axios" },
    { id: "10", name: "拾色器", url: "http://casesandberg.github.io/react-color/", desc: "React-color" }
  ];

  const systemLabelItems = [
    { id: "1", name: "管理系统", color: "success" },
    { id: "2", name: "前后端分离", color: "processing" },
    { id: "3", name: "微服务架构", color: "processing" },
    { id: "4", name: "React", color: "error" },
    { id: "5", name: "Umi", color: "warning" },
    { id: "6", name: "Dva", color: "default" },
    { id: "7", name: "服务治理", color: "success" },
    { id: "8", name: "服务网关", color: "success" },
    { id: "9", name: "配置中心", color: "processing" },
    { id: "10", name: "调佣链分析", color: "warning" },
    { id: "11", name: "数据库版本管理", color: "processing" },
    { id: "12", name: "Ant Design", color: "error" },
    { id: "13", name: "AntV", color: "warning" },
    { id: "14", name: "PostgreSql", color: "default" },
    { id: "15", name: "Redis", color: "success" },
    { id: "16", name: "Mybatis", color: "processing" },
    { id: "17", name: "Druid", color: "error" },
    { id: "18", name: "Flyway", color: "warning" },
    { id: "19", name: "动态数据集", color: "default" },
    { id: "20", name: "Apache Shiro", color: "warning" },
    { id: "21", name: "Jwt", color: "processing" },
    { id: "22", name: "Docker", color: "success" },
    { id: "23", name: "容器部署", color: "warning" },
    { id: "24", name: "Selenium", color: "default" },
    { id: "25", name: "RabbitMQ", color: "success" },
    { id: "26", name: "ElasticSearch", color: "processing" },
    { id: "27", name: "SpringBoot自动配置", color: "default" },
    { id: "28", name: "Aop操作日志", color: "success" },
    { id: "29", name: "资源监控", color: "warning" },
  ];

  return (
    <Layout style={{ padding: "15px 15px 15px 15px", background: '#fff'}}>
      <Header style={{ background: '#e8e8e88a', padding: 0, marginBottom: "15px", width: "100%", height: "60px" }}>
        <div style={{ padding: "10px 15px", display: "inline" }}>
          <i className="ri-global-fill" style={{ fontSize: "35px", color: "blue" }} />
          <span style={{ padding: "0px 10px", position: "absolute", fontSize: "20px" }}>{config.NAME}</span>
        </div>
      </Header>
      <Content>
        <div className={styles.infoDiv}>
          <div className={styles.introductionDiv}>
            <div className={styles.infoShowDiv}>
              <div className={styles.briefDiv}>
                <div className={styles.infoFontSize}><i className="ri-bookmark-3-fill" style={iconTitleStyle}></i>系统简介</div>
                <Divider className={styles.dividerDiv} />
              </div>
              <div className={styles.briefContentDiv}>
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{config.NAME}是一套基于前后端分离架构的后台管理系统。kunlun-web 基于React + Umi(乌米) + Ant Design (蚂蚁金服) 构建开发，提供前端解决方案；kunlun-service 基于 SpringBoot 与 Spring Cloud 构建开发，提供后端基于微服务架构的解决方案。系统通过Apache Shiro与Jwt组件，用token进行数据交互认证，可快速开发并独立进行Docker容器化部署。
              </div>
            </div>
            <div className={styles.projectDiv}>
              <div className={styles.briefDiv}>
                <div className={styles.infoFontSize}><i className="ri-layout-masonry-fill" style={iconTitleStyle}></i>项目模块</div>
                <Divider className={styles.dividerDiv} />
              </div>
              <div className={styles.projectInfoDIv}>
                <Table pagination={false} bordered={true} tableLayout={"fixed"} columns={describeColumns} dataSource={descriptionList} />
              </div>
            </div>
            <div className={styles.logDiv}>
              <div className={styles.logLeftDiv}>
                <div className={styles.logFontDiv}>
                  <div className={styles.infoFontSize}><i className="ri-stack-fill" style={iconTitleStyle}></i>系统升级日志</div>
                  <Divider className={styles.dividerDiv} />
                </div>
                <div className={styles.logContenDiv}>
                  <Timeline>
                    {
                      timeLineItems.map(item =>
                        <TimeLineItem dot={<i className={item.icon} style={iconStyle}></i>} color={item.color}>{item.desc}</TimeLineItem>
                      )
                    }
                  </Timeline>
                </div>
              </div>
              <div className={styles.logRightDiv}>
                <div className={styles.versionDiv}>
                  <div className={styles.webInfoDiv}>
                    <div className={styles.infoFontSize}>
                      <i className="ri-map-2-fill" style={iconTitleStyle}></i>版本信息
                    </div>
                    <Divider className={styles.dividerDiv} />
                  </div>
                  <div className={styles.webDetailDiv}>
                    <Descriptions title="" layout="horizontal" bordered column={1} className={styles.technicalDescription} style={{ flex: 1}}>
                      <DescriptionsItem label="当前版本">kunlun-admin V1.5.0</DescriptionsItem>
                      <DescriptionsItem label="开发成员">野性形态、xuesjie</DescriptionsItem>
                      <DescriptionsItem label="代码仓库">
                        <span><a target = "_blank" href={"https://gitee.com/xuesjie/kunlun-web"}><Tag color="#f50">WEB前端</Tag></a></span>
                        <span style={{ marginLeft: "0px" }}>
                          <a target = "_blank" href={"https://gitee.com/xuesjie/kunlun-service"}><Tag color="#108ee9">JAVA后端</Tag></a>
                        </span>
                      </DescriptionsItem>
                    </Descriptions>
                  </div>
                </div>
                <div className={styles.labelDiv}>
                  <div className={styles.linkFontDiv}>
                    <div className={styles.infoFontSize}>
                      <i className="ri-centos-fill" style={iconTitleStyle}></i>系统标签
                    </div>
                  </div>
                  <div className={styles.labelShowDiv}>
                    {
                      systemLabelItems.map(item =>
                        <Tag color={item.color} style={{ margin: "0px 5px 5px 0px" }}>{item.name}</Tag>)
                    }
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div className={styles.structureDiv}>
            <div className={styles.serviceDiv}>
              <div className={styles.serviceInfoDiv}>
                <div className={styles.infoFontSize}><i className="ri-code-box-fill" style={iconTitleStyle}></i>后端技术</div>
                <Divider className={styles.dividerDiv} />
              </div>
              <div className={styles.serviceDetailDiv}>
                <Descriptions title="" layout="horizontal" bordered column={1} className={styles.technicalDescription} style={{ flex: 1}}>
                  <DescriptionsItem label="核心框架"><a target = "_blank" href={"https://spring.io/projects/spring-boot"}>Spring Boot</a></DescriptionsItem>
                  <DescriptionsItem label="服务架构"><a target = "_blank" href={"https://spring.io/projects/spring-cloud"}>Spring Cloud</a></DescriptionsItem>
                  <DescriptionsItem label="安全框架"><a target = "_blank" href={"http://shiro.apache.org/"}>apache Shiro</a>、<a target = "_blank" href={"https://jwt.io/"}>Jwt</a></DescriptionsItem>
                  <DescriptionsItem label="持久层框架"><a target = "_blank" href={"http://www.mybatis.org/mybatis-3/zh/index.html"}>MyBatis</a></DescriptionsItem>
                  <DescriptionsItem label="数据库连接池"><a target = "_blank" href={"https://github.com/alibaba/druid"}>Druid</a></DescriptionsItem>
                  <DescriptionsItem label="数据库">PosgreSQL、<a target = "_blank" href={"https://redis.io/"}>Redis</a></DescriptionsItem>
                  <DescriptionsItem label="工作流引擎"><a target = "_blank" href={"https://www.activiti.org/documentation"}>Activiti-5.22.0</a></DescriptionsItem>
                  <DescriptionsItem label="脚本执行">Flyway</DescriptionsItem>
                  <DescriptionsItem label="资源爬取"><a target = "_blank" href={"https://www.selenium.dev/"}>Selenium</a></DescriptionsItem>
                  <DescriptionsItem label="消息组件"><a target = "_blank" href={"https://www.rabbitmq.com/"}>RabbitMQ</a></DescriptionsItem>
                  <DescriptionsItem label="全局搜索"><a target = "_blank" href={"https://www.elastic.co/"}>ElasticSearch</a></DescriptionsItem>
                </Descriptions>
              </div>
            </div>
            <div className={styles.webDiv}>
              <div className={styles.webInfoDiv}>
                <div className={styles.infoFontSize}><i className="ri-html5-fill" style={iconTitleStyle}></i>前端技术</div>
                <Divider className={styles.dividerDiv} />
              </div>
              <div className={styles.webDetailDiv}>
                <Descriptions title="" layout="horizontal" bordered column={1} className={styles.technicalDescription} style={{ flex: 1}}>
                  {
                    webItems.map(item =>
                      <DescriptionsItem label={item.name}>
                        <a target = "_blank" href={item.url}>{item.desc}</a>
                      </DescriptionsItem>
                    )
                  }
                </Descriptions>
              </div>
            </div>
            <div className={styles.linkDiv}>
              <div className={styles.linkFontDiv}>
                <div className={styles.infoFontSize}>
                  <i className="ri-base-station-fill" style={iconTitleStyle}></i>友情链接
                </div>
              </div>
              <div className={styles.linkUrlDiv}>
                <Carousel dotPosition={"bottom"} className={styles.carousel} autoplay={true}>
                  {
                    linkUrlItems.map(item =>
                      <div id={"linkUrl" + item.id} className={item.className}>
                        <h3 className={styles.carsouselFont}>
                          <a href={item.url} target="_blank">{item.desc}</a>
                        </h3>
                      </div>)
                  }
                </Carousel>
              </div>
            </div>
          </div>
        </div>
      </Content>
      <Footer style={{ textAlign: 'center', marginTop: "15px", width: "100%", height: "60px", padding: "20px 0px" }}>
        <div>{config.FOOTER_TEXT}</div>
      </Footer>
    </Layout>
  );
};

export default SystemInfo;
