import React from 'react';
import { Modal, Form, Input, Row, Col, Select } from 'antd';
import config from '../../../config/config';
import styles from './Navigation.less';
import { SketchPicker } from 'react-color';

const NavigationModal = (props) => {

  const {
    navigateModalVisible, onCancel, onSave, onChangeColor,
  } = props;

  const onOk = () => {
    validateFields().then(values => {
      onSave(values);
    }).catch(error => {
      console.log("===== 登陆验证失败 =====");
    });
  };

  return (
    <div>
      <Modal
        className={styles.modal}
        visible={navigateModalVisible}
        title={"设置图标颜色"}
        okText="保存"
        onCancel={onCancel}
        onOk={onOk}
        width={500}
        destroyOnClose={true}
      >
        <SketchPicker style={{ marginBottom: "15px" }} onChangeComplete={(e) => onChangeColor("select", e.hex)} />
      </Modal>
    </div>
  );
};

export default NavigationModal;
