import React from 'react';
import { Card, Row, Col, Drawer, Spin, Statistic } from 'antd';
import config from '../../../config/config';
import styles from './Machine.less';
import { Line, DualAxes } from '@ant-design/charts';
import moment from 'moment';

class MachineDrawer extends React.Component {

  constructor(props) {
    super(props);
  }

  render() {

    const {
      machineModalVisible, onCancel, saveLoading, machineInstanceRecord, machineLineData
    } = this.props;

    const dateFormatter = "yyyy-MM-DD HH:mm:ss";
    const netSendData = [], netReceiveData = [];
    for (let i = 0; i < machineLineData.length; i++) {
      const record = machineLineData[i];
      const {
         cpuUsedPercent, memoryUsedPercent, diskUsedPercent, jvmUsedPercent,
        netReceiveKbCount, netSendKbCount, createTime
      } = record;
      record.createTime = moment(createTime).format(dateFormatter);
      record["CPU使用率"] = cpuUsedPercent;
      record["内存使用率"] = memoryUsedPercent;
      record["磁盘使用率"] = diskUsedPercent;
      record["JVM内存使用率"] = jvmUsedPercent;
      netReceiveData.push({ createTime: record.createTime, labelName: "每秒接收KB数", netReceiveKbCount });
      netSendData.push({ createTime: record.createTime, labelName: "每秒发送KB数", netSendKbCount });
    }

    const buildLineConfig = (field) => {
      return {
        padding: [5, 0, 20, 40],
        data: machineLineData && machineLineData.length > 0 ? machineLineData : [],
        xField: 'createTime',
        yField: field,
        point: {
          size: 3,
          shape: 'diamond',
          style: {
            fill: 'white',
            stroke: '#5B8FF9',
            lineWidth: 1,
          },
        },
      };
    }

    const cpuUsedPercentConfig = buildLineConfig("CPU使用率");
    const memoryUsedPercentConfig = buildLineConfig("内存使用率");
    const diskUsedPercentConfig = buildLineConfig("磁盘使用率");
    const jvmUsedPercentConfig = buildLineConfig("JVM内存使用率");

    const netDataConfig = {
      data: [netReceiveData, netSendData],
      xField: 'createTime',
      yField: ['netReceiveKbCount', 'netSendKbCount'],
      geometryOptions: [
        {
          geometry: 'line',
          seriesField: 'labelName',
          lineStyle: {
            lineWidth: 3,
            lineDash: [5, 5],
          },
          smooth: true,
        },
        {
          geometry: 'line',
          seriesField: 'labelName',
          point: {},
        },
      ],
    }

    const netDataValue = machineInstanceRecord.netReceiveKbCount + "/" + machineInstanceRecord.netSendKbCount;

    return (
      <Spin spinning={saveLoading}>
        <Drawer
          title="资源详情"
          width={700}
          placement="right"
          onClose={onCancel}
          maskClosable={false}
          visible={machineModalVisible}
        >
          <Row gutter={15}>
            <Col span={12}>
              <Card bodyStyle={{ padding: "15px" }}>
                <div>
                  <span>服务名</span>
                  <span style={{ float: "right" }}>{machineInstanceRecord.serviceName}</span>
                </div>
                <div style={{ marginTop: "10px" }}>
                  <span>操作系统</span>
                  <span style={{ float: "right" }}>{machineInstanceRecord.systemName}</span>
                </div>
                <div style={{ marginTop: "10px" }}>
                  <span>IP地址</span>
                  <span style={{ float: "right" }}>{machineInstanceRecord.ipAddress}</span>
                </div>
                <div style={{ marginTop: "10px" }}>
                  <span>系统架构</span>
                  <span style={{ float: "right" }}>{machineInstanceRecord.systemArchitecture}</span>
                </div>
              </Card>
            </Col>
            <Col span={12}>
              <Card bodyStyle={{ padding: "15px" }}>
                <div>
                  <span>JDK名</span>
                  <span style={{ float: "right" }}>{machineInstanceRecord.jdkName}</span>
                </div>
                <div style={{ marginTop: "10px" }}>
                  <span>JDK版本</span>
                  <span style={{ float: "right" }}>{machineInstanceRecord.jdkVersion}</span>
                </div>
                <div style={{ marginTop: "10px" }}>
                  <span>JVM启动时间</span>
                  <span style={{ float: "right" }}>{machineInstanceRecord.jvmStartTime && moment(machineInstanceRecord.jvmStartTime).format(dateFormatter)}</span>
                </div>
                <div style={{ marginTop: "10px" }}>
                  <span>JVM运行时间</span>
                  <span style={{ float: "right" }}>{machineInstanceRecord.jvmUpTime}</span>
                </div>
              </Card>
            </Col>
          </Row>
          <Row gutter={15} style={{ marginTop: "15px", textAlign: "center" }}>
            <Col span={8}>
              <Card bodyStyle={{ padding: "15px" }}>
                <Statistic title="CPU使用率" valueStyle={{ color: '#ec0425' }}
                           value={machineInstanceRecord.cpuUsedPercent} precision={2} suffix="%"
                />
              </Card>
            </Col>
            <Col span={8}>
              <Card bodyStyle={{ padding: "15px" }}>
                <Statistic title="内存使用率" valueStyle={{ color: '#09d91a' }}
                           value={machineInstanceRecord.memoryUsedPercent} precision={2} suffix="%"
                />
              </Card>
            </Col>
            <Col span={8}>
              <Card bodyStyle={{ padding: "15px" }}>
                <Statistic title="磁盘使用率" valueStyle={{ color: '#160abd' }}
                           value={machineInstanceRecord.diskUsedPercent} precision={2} suffix="%"
                />
              </Card>
            </Col>
          </Row>
          <Row gutter={15} style={{ marginTop: "15px", textAlign: "center" }}>
            <Col span={12}>
              <Card bodyStyle={{ padding: "15px" }}>
                <Statistic title="JVM内存使用率" valueStyle={{ color: '#160abd' }}
                           value={machineInstanceRecord.jvmUsedPercent} precision={2} suffix="%"
                />
              </Card>
            </Col>
            <Col span={12}>
              <Card bodyStyle={{ padding: "15px" }}>
                <Statistic title="每秒接收/发送KB数" valueStyle={{ color: '#160abd' }}
                           value={netDataValue} precision={2} suffix=""
                />
              </Card>
            </Col>
          </Row>
          <Row style={{ marginTop: "15px" }}>
            <Card style={{ width: "100%" }} bodyStyle={{ padding: "15px" }}>
              <div className={styles.userStatisticsRightDiv}>
                <Line {...cpuUsedPercentConfig} />
              </div>
            </Card>
          </Row>
          <Row style={{ marginTop: "15px" }}>
            <Card style={{ width: "100%" }} bodyStyle={{ padding: "15px" }}>
              <div className={styles.userStatisticsRightDiv}>
                <Line {...memoryUsedPercentConfig} />
              </div>
            </Card>
          </Row>
          <Row style={{ marginTop: "15px" }}>
            <Card style={{ width: "100%" }} bodyStyle={{ padding: "15px" }}>
              <div className={styles.userStatisticsRightDiv}>
                <Line {...diskUsedPercentConfig} />
              </div>
            </Card>
          </Row>
          <Row style={{ marginTop: "15px" }}>
            <Card style={{ width: "100%" }} bodyStyle={{ padding: "15px" }}>
              <div className={styles.userStatisticsRightDiv}>
                <Line {...jvmUsedPercentConfig} />
              </div>
            </Card>
          </Row>
          <Row style={{ marginTop: "15px" }}>
            <Card style={{ width: "100%" }} bodyStyle={{ padding: "15px" }}>
              <div className={styles.userStatisticsRightDiv}>
                <DualAxes {...netDataConfig} />
              </div>
            </Card>
          </Row>
        </Drawer>
      </Spin>
    );
  };
}

export default MachineDrawer;
