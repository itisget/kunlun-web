import React from 'react';
import { Button, Upload } from 'antd';

const PictureToolBar = (props) => {

  const { onUpload, onDelete } = props;

  const iconStyle = {
    verticalAlign: "bottom",
    marginRight: "5px",
  }

  const uploadProps = {
    name: "PictureToolBar",
    showUploadList: false,
    beforeUpload: (file) => {
      const formData = new FormData();
      formData.append("file", file);
      onUpload(formData);
    },
  }

  return (
    <div style={{ marginBottom: "15px", marginTop: "15px", display: "flex" }}>
      <Upload { ...uploadProps }>
        <Button type="primary" size="default" icon={<i className="ri-refresh-line" style={iconStyle}></i>}>上传</Button>
      </Upload>
      <Button type="dashed" size="default" icon={<i className="ri-download-2-line" style={iconStyle}></i>} style={{ marginLeft: "15px" }} onClick={onDelete}>删除</Button>
    </div>
  );
}

export default PictureToolBar;
