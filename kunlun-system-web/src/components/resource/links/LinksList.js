import React from 'react';
import styles from './Links.less';
import moment from 'moment';
import { Spin, Popconfirm, Tooltip, Input } from 'antd';

const LinksList = (props) => {

  const {
    systemLinkLoading, customLinkLoading, systemLinkList, customLinkList, editVisible,
    currentLinkName, onShowOperate, onRefresh, onAdd, onEdit, onDelete, onSearch
  } = props;

  const iconStyle = {
    verticalAlign: "bottom",
    marginRight: "5px",
  }

  const onLinkPage = (item) => {
    console.log("LinksPage onLinkPage => ", item);
    window.open(item.url);
  }

  const generateLinkCard = (linkList, linkType) => {
    if (!linkList || (linkList && linkList.length == 0)) {
      console.log("LinksList generateLinkCard Empty");
      return null;
    }

    const linksDom = document.getElementById("root");
    const linkCardWidth = linksDom.clientWidth / 5 - (linkList && linkList.length > 10 ? 17 / 5 : 0);

    let rootDom = null;
    let resultLinkCard = new Array(), linkCardElement = new Array();
    for (let i = 0; i < linkList.length; i++) {
      const item = linkList[i];
      const itemElement = (
        <div style={{ width: linkCardWidth + "px" }} className={styles.functionDiv} onMouseOver={() => onShowOperate(item, true)} onMouseLeave={() => onShowOperate(item, false)}>
          <div style={{ margin: "-20px -12% -15px -15px", float: "right", display: currentLinkName && currentLinkName == item.name && editVisible ? "block" : "none" }}>
            <span onClick={() => onEdit(item, linkType)}>
              <i className='ri-file-edit-line' style={{ verticalAlign: "sub", marginRight: "5px", fontSize: "17px" }} />
            </span>
            <span>
              <Popconfirm title="是否确认删除此链接？" onConfirm={onDelete.bind(null, item, linkType)}>
                <Tooltip title={"删除"}>
                  <i className='ri-delete-bin-line' style={{ verticalAlign: "sub", marginRight: "5px", fontSize: "17px" }} />
                </Tooltip>
              </Popconfirm>
            </span>
          </div>
          <div onClick={() => onLinkPage(item)}>
            <i className={item.icon} style={{ verticalAlign: "middle", marginRight: "5px", fontSize: "18px", color: item.color }} />
            {item.name}
          </div>
          <div>{moment(item.createTime).format("YYYY-MM-DD HH:MM:ss")}</div>
        </div>
      );
      linkCardElement.push(itemElement);

      if ((i + 1) % 5 == 0 || ((i + 1) % 5 != 0) && linkList.length - 1 == i) {
        rootDom =
          <div style={{ width: "100%", height: "95px", display: "flex", flexDirection: "row" }}>
            { linkCardElement }
          </div>;
        resultLinkCard.push(rootDom);
        linkCardElement = new Array();
      }
    }
    return resultLinkCard;
  }

  return (
    <div style={{ width: "100%", height: "100%" }}>
      <div style={{ width: "100%" }}>
        <Spin spinning={false}>
          <div style={{ width: "calc(100% - 15px)", height: "40px", background: "rgba(232, 232, 232, 0.54)", padding: "15px 0px 0px 15px", display: "flex", flexDirection: "row" }}>
            <div style={{fontWeight: "800"}}><i className="ri-links-fill" style={iconStyle}></i>系统链接</div>
            <div style={{ margin: "-25px 0px 0px 65%" }} className={styles.linkDiv}>
              <span onClick={() => onRefresh("system")} className={styles.spanDiv}>
                <Tooltip title={"刷新"}>
                  <i className='ri-refresh-line' style={{ verticalAlign: "text-bottom", marginRight: "5px", fontSize: "17px" }} />
                </Tooltip>
              </span>
              <span onClick={() => onAdd("system")} className={styles.spanDiv}>
                <Tooltip title={"新增"}>
                  <i className='ri-add-line' style={{ marginRight: "5px", fontSize: "23px" }} />
                </Tooltip>
              </span>
              <span className={styles.spanDiv}>
                <Input.Search placeholder="请输入链接名" onSearch={(e) => onSearch(e, "system")} enterButton />
              </span>
            </div>
          </div>
          <div style={{ height: "190px", overflowY: "scroll" }}>
            { generateLinkCard(systemLinkList, "system") }
          </div>
        </Spin>
      </div>
      <div style={{ width: "100%" }}>
        <Spin spinning={customLinkLoading}>
          <div style={{ width: "calc(100% - 15px)", height: "40px", background: "rgba(232, 232, 232, 0.54)", padding: "15px 0px 0px 15px", display: "flex", flexDirection: "row" }}>
            <div style={{fontWeight: "800"}}><i className="ri-links-fill" style={iconStyle}></i>外部链接</div>
            <div style={{ margin: "-25px 0px 0px 65%" }} className={styles.linkDiv}>
              <span onClick={() => onRefresh("custom")} className={styles.spanDiv}>
                <Tooltip title={"刷新"}>
                  <i className='ri-refresh-line' style={{ verticalAlign: "text-bottom", marginRight: "5px", fontSize: "17px" }} />
                </Tooltip>
              </span>
              <span onClick={() => onAdd("custom")} className={styles.spanDiv}>
                <Tooltip title={"新增"}>
                  <i className='ri-add-line' style={{ verticalAlign: "-1px", marginRight: "5px", fontSize: "23px" }} />
                </Tooltip>
              </span>
              <span className={styles.spanDiv}>
                <Input.Search placeholder="请输入链接名" onSearch={(e) => onSearch(e, "custom")} enterButton />
              </span>
            </div>
          </div>
          <div style={{ height: "180px", overflowY: "scroll" }}>
            { generateLinkCard(customLinkList, "custom") }
          </div>
        </Spin>
      </div>
    </div>
  );
}

export default LinksList;
