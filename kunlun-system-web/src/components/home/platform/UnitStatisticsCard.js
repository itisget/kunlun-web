import React from 'react';
import {Tooltip} from 'antd';
import { Column } from '@ant-design/charts';
import indexStyles from "../../../pages/home/Home.less";

class UnitStatisticsCard extends React.Component {

  render() {

    const {onShowDetail} = this.props;

    const unitData = [
      { key: "company", type: '单位', sales: 3, color: "#08de26" },
      { key: "department", type: '部门', sales: 15, color: "#e30b04" },
      { key: "post", type: '岗位', sales: 38, color: "#0753e5" },
      { key: "role", type: '角色', sales: 13, color: "#bc0bee" },
      { key: "user", type: '用户', sales: 100, color: "#ec7f03" },
    ];

    const config = {
      data: unitData,
      xField: 'type',
      yField: 'sales',
      color: ({ type }) => {
        let backgroundColor = unitData[4].color;
        switch (type) {
          case "单位": {
            backgroundColor = unitData[0].color;
            break;
          }
          case "部门": {
            backgroundColor = unitData[1].color;
            break;
          }
          case "岗位": {
            backgroundColor = unitData[2].color;
            break;
          }
          case "角色": {
            backgroundColor = unitData[3].color;
            break;
          }
        }
        return backgroundColor;
      },
      label: {
        // 可手动配置 label 数据标签位置
        position: 'middle',
        // 'top', 'bottom', 'middle',
        // 配置样式
        style: {
          fill: '#FFFFFF',
          opacity: 0.6,
        },
      },
      xAxis: {
        label: {
          autoHide: true,
          autoRotate: false,
        },
      },
      meta: {
        type: {
          alias: '类别',
        },
        sales: {
          alias: '数量（个）',
        },
      },
    };

    return (
      <>
        <div className={indexStyles.todo_audit_card_title}>
          <div className={indexStyles.new_notice_title_font}>组织统计</div>
          <div className={indexStyles.new_notice_show}>
            <div onClick={() => onShowDetail("notice")} className={indexStyles.new_notice_show_icon}>
              <Tooltip title={"查看详情"}>
                <i className="ri-chat-poll-line" style={{fontSize: "19px", marginRight: "15px"}}></i>
              </Tooltip>
            </div>
          </div>
        </div>
        <div className={indexStyles.unit_statistics_card_content} style={{ height: "150px", background: "#FFFFFF" }}>
          <Column {...config} />
        </div>
      </>);
  };
}

export default UnitStatisticsCard;
