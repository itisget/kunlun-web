import React from 'react';
import styles from '../Home.less';
import { Rate, Tooltip, Flex, Progress } from 'antd';
import indexStyles from '../../../pages/home/Home.less';

class UserEstimateCard extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      activedKey: ["1"]
    }
  }

  render() {

    const { onShowDetail } = this.props;

    const rateValue = 4.5;

    return (
      <div>
        <div className={styles.scheduleDiv}>
          <div className={indexStyles.mqTitleFontDiv}>用户评价</div>
          <div onClick={() => onShowDetail("schedule")} className={indexStyles.fontWeightHover}>
            <Tooltip title={"查看详情"}>
              <i className="ri-flag-line" style={{fontSize: "19px", marginRight: "15px"}}></i>
            </Tooltip>
          </div>
        </div>
        <div className={styles.user_estimate_card_content}>
          <div style={{ display: "flex", flexDirection: "row" }}>
            <div style={{ flex: 1, fontSize: 20 }}>{rateValue}</div>
            <div style={{ flex: 9, padding: "8px 0px 0px 0px" }}>
              <Rate allowHalf defaultValue={rateValue} />
            </div>
          </div>
          <Flex gap="small" vertical>
            <div style={{ display: "flex", flexDirection: "row", margin: "-6px 0px -5px 0px" }}>
              <div style={{ flex: 2.5 }}>
                <Progress percent={80} size={"small"} showInfo={false} />
              </div>
              <div style={{ flex: 1, padding: "0px 0px 0px 10px", color: "#696767" }}>
                <i className="ri-star-line" /> 5：85人
              </div>
            </div>
            <div style={{ display: "flex", flexDirection: "row", margin: "-6px 0px -5px 0px" }}>
              <div style={{ flex: 2.5 }}>
                <Progress percent={50} size={"small"} status="active" showInfo={false} />
              </div>
              <div style={{ flex: 1, padding: "0px 0px 0px 10px", color: "#696767" }}>
                <i className="ri-star-line" /> 4：55人
              </div>
            </div>
            <div style={{ display: "flex", flexDirection: "row", margin: "-6px 0px -5px 0px" }}>
              <div style={{ flex: 2.5 }}>
                <Progress percent={30} size={"small"} status="exception" showInfo={false} />
              </div>
              <div style={{ flex: 1, padding: "0px 0px 0px 10px", color: "#696767" }}>
                <i className="ri-star-line" /> 3：35人
              </div>
            </div>
            <div style={{ display: "flex", flexDirection: "row", margin: "-6px 0px -5px 0px" }}>
              <div style={{ flex: 2.5 }}>
                <Progress percent={10} size={"small"} showInfo={false} />
              </div>
              <div style={{ flex: 1, padding: "0px 0px 0px 10px", color: "#696767" }}>
                <i className="ri-star-line" /> 2：15人
              </div>
            </div>
            <div style={{ display: "flex", flexDirection: "row", margin: "-6px 0px -5px 0px" }}>
              <div style={{ flex: 2.5 }}>
                <Progress percent={5} size={"small"} showInfo={false} />
              </div>
              <div style={{ flex: 1, padding: "0px 0px 0px 10px", color: "#696767" }}>
                <i className="ri-star-line" /> 1：5人
              </div>
            </div>
          </Flex>
        </div>
      </div>
    );
  };
}

export default UserEstimateCard;
