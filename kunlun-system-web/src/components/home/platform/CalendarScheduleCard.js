import React from 'react';
import styles from '../Home.less';
import { Calendar, Empty } from 'antd';
import moment from 'moment';
import indexStyles from '../../../pages/home/Home.less';
import locale from 'antd/es/date-picker/locale/zh_CN';

class CalendarScheduleCard extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      activedKey: ["1"]
    }
  }

  render() {

    const {scheduleData, scheduleIndex, scheduleTotal, onShowDetail, onClickArrow, scheduleList, onCacheCalenarMode} = this.props;

    const dateFormat = "YYYY年MM月DD日";
    const timeFormat = "HH时mm分";

    const allScheduleList = [
      {id: "11111", theme: "技术选型会议", themeColor: "red", startTime: "2020-04-05 00:00:00", endTime: "2019-11-07 00:00:00", location: "会议室2019", participant: "11111", content: "1111111111"},
      {id: "22222", theme: "小组讨论", themeColor: "blue", startTime: "2020-04-05 00:00:00", endTime: "2019-11-08 00:00:00", location: "会议室2019", participant: "22222", content: "2222222222"},
      {id: "33333", theme: "代码评审", themeColor: "green", startTime: "2020-04-05 00:00:00", endTime: "2019-11-17 00:00:00", location: "会议室2019", participant: "33333", content: "3333333333"},
      {id: "44444", theme: "技术选型会议", themeColor: "purple", startTime: "2020-04-05 00:00:00", endTime: "2019-11-17 00:00:00", location: "会议室2019", participant: "44444", content: "4444444444"},
      {id: "55555", theme: "小组讨论", themeColor: "blue", startTime: "2020-04-05 00:00:00", endTime: "2019-11-27 00:00:00", location: "会议室2019", participant: "55555", content: "5555555555"},
    ];

    const dateCellRender = (value) => {
      const listData = scheduleList.filter(item => value.year() == moment(item.startTime).year() && value.month() == moment(item.startTime).month() && value.date() == moment(item.startTime).date());
      return (
          <ul className={styles.events}>
            {
              listData.map(item => (
                  <li key={item.id}>
                    <i className={moment(item.endTime) < new Date() ? "ri-forbid-line" : "ri-play-circle-line"} style={{fontSize: "16px", color: item.themeColor, verticalAlign: "sub", marginRight: "5px",}}></i>
                    &nbsp;&nbsp;{item.theme}
                  </li>
              ))
            }
          </ul>
      );
    };

    const monthCellRender = (value) => {
      const listDatas = scheduleList.filter(item => value.year() == moment(item.startTime).year() && value.month() == moment(item.startTime).month());
      const num = listDatas.length;
      return num ? (
          <div className={styles.notesMonth}>
            <section style={{fontWeight: "bolder"}}>{num}</section>
          </div>
      ) : null;
    };

    const onSelect = (value) => {
      let selectedSchedules;
      if (calendarMode == "month") {
        selectedSchedules = scheduleList.filter(item => value.year() == moment(item.startTime).year() && value.month() == moment(item.startTime).month() && moment(item.startTime).date() == value.date());
      } else {
        selectedSchedules = scheduleList.filter(item => value.year() == moment(item.startTime).year() && moment(item.startTime).month() == value.month());
      }
      if (selectedSchedules && selectedSchedules.length > 0) {
        onShowDetail(selectedSchedules);
      }
    };

    return (
      <>
        <div className={indexStyles.calendar_schedule_card_content}>
          <div className={indexStyles.calendar_schedule_card_title}>事项日程</div>
          <Calendar
              locale={locale}
              className={indexStyles.scheduleTable}
              dateCellRender={dateCellRender}
              monthCellRender={monthCellRender}
              onSelect={e => onSelect(e)}
              onPanelChange={onCacheCalenarMode}
          />
        </div>
        <div className={styles.scheduleShowDiv}>
          <div className={styles.scheduleToolBarDiv}>
            <div>{moment(new Date()).format(dateFormat)}&nbsp;&nbsp;&nbsp;&nbsp;第&nbsp;{scheduleTotal == 0 ? 0 : scheduleIndex + 1}&nbsp;条，共&nbsp;{scheduleTotal}&nbsp;条</div>
          </div>
          {
            !scheduleData ? <Empty /> :
              <div className={styles.scheduleContentDiv}>
                <div className={styles.arrowLeft} onClick={() => onClickArrow("left")}>
                  <i className="ri-arrow-left-circle-line"></i>
                </div>
                <div>
                  <div><span>主题：</span><span style={{color: scheduleData.themeColor}}>&nbsp;{scheduleData.theme}</span></div>
                  <div style={{display: "flex", flexDirection: "row"}}>
                    <div style={{display: "flex", flex: 1}}><span>时间：</span><span>&nbsp;{moment(scheduleData.startTime).format(timeFormat) + " - " + moment(scheduleData.endTime).format(timeFormat)}</span></div>
                    <div style={{display: "flex", flex: 1}}><span>地点：</span><span>&nbsp;{scheduleData.location}</span></div>
                  </div>
                  <div><span>内容：</span><span>&nbsp;{scheduleData.content}</span></div>
                  <div className={styles.scheduleShowPersonDiv}>
                    <span>人员：</span>
                    <span className={styles.personDiv}>{scheduleData.participant}</span>
                  </div>
                </div>
                <div className={styles.arrowRight} onClick={() => onClickArrow("right")}>
                  <i className="ri-arrow-right-circle-line"></i>
                </div>
              </div>
          }
        </div>
      </>
    );
  };
}

export default CalendarScheduleCard;
