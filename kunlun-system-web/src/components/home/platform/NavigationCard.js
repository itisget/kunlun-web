import React from 'react';
import styles from '../Home.less';
import {Tooltip} from 'antd';
import indexStyles from "../../../pages/home/Home.less";

class NavigationCard extends React.Component {

  render() {

    const {onShowDetail} = this.props;

    return (
      <div style={{ width: "100%", height: "100%" }}>
        <div className={styles.navigateTitleDiv}>
          <div className={styles.navigateleFontDiv}>便捷导航</div>
          <div onClick={() => onShowDetail("navigate")} className={indexStyles.fontWeightHover}>
            <Tooltip title={"自定义导航"}>
              <i className="ri-edit-circle-line" style={{fontSize: "19px", marginRight: "15px"}}></i>
            </Tooltip>
          </div>
        </div>
        <div className={styles.commonFunctionDiv}>
          <div className={styles.functionDiv} onClick={() => onShowDetail("list")}>
            <div className={styles.functionIconDiv}><i className="ri-account-box-line" style={{fontSize: "25px", color: "red"}}></i></div>用户管理
          </div>
          <div className={styles.functionDiv} onClick={() => onShowDetail("amap")}>
            <div className={styles.functionIconDiv}><i className="ri-map-pin-5-line" style={{fontSize: "25px", color: "blue"}}></i></div>用户地图
          </div>
          <div className={styles.functionDiv} onClick={() => onShowDetail("model")}>
            <div className={styles.functionIconDiv}><i className="ri-door-lock-box-line" style={{fontSize: "25px", color: "green"}}></i></div>模型管理
          </div>
          <div className={styles.functionDiv} onClick={() => onShowDetail("process")}>
            <div className={styles.functionIconDiv}><i className="ri-equalizer-line" style={{fontSize: "25px", color: "#ff9800"}}></i></div>流程管理
          </div>
          <div className={styles.functionDiv} onClick={() => onShowDetail("zipkin")}>
            <div className={styles.functionIconDiv}><i className="ri-file-search-line" style={{fontSize: "25px", color: "#7ffb42"}}></i></div>调用追踪
          </div>
          <div className={styles.functionDiv} onClick={() => onShowDetail("rabbitmq")}>
            <div className={styles.functionIconDiv}><i className="ri-pages-line" style={{fontSize: "25px", color: "#795548"}}></i></div>MQ管理
          </div>
          <div className={styles.functionDiv} onClick={() => onShowDetail("log")}>
            <div className={styles.functionIconDiv}><i className="ri-file-settings-line" style={{fontSize: "25px", color: "#9c27b0"}}></i></div>操作日志
          </div>
          <div className={styles.functionDiv} onClick={() => onShowDetail("schedule")}>
            <div className={styles.functionIconDiv}><i className="ri-article-line" style={{fontSize: "25px", color: "#000000"}}></i></div>日程管理
          </div>
          <div className={styles.functionDiv} onClick={() => onShowDetail("notice")}>
            <div className={styles.functionIconDiv}><i className="ri-message-3-line" style={{fontSize: "25px", color: "#03a9f4"}}></i></div>通知公告
          </div>
          <div className={styles.functionDiv} onClick={() => onShowDetail("menu")}>
            <div className={styles.functionIconDiv}><i className="ri-function-line" style={{fontSize: "25px", color: "#922ac5"}}></i></div>菜单管理
          </div>
        </div>
      </div>
    );
  };
}

export default NavigationCard;
