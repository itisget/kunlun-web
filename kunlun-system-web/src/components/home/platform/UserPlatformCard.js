import React from 'react';
import {Avatar, Card, Col, Row, Statistic, Tooltip} from 'antd';
import indexStyles from "../../../pages/home/Home.less";
import userLogo from '../../../assets/userLogo.png';
import styles from '../Home.less';

class UserPlatformCard extends React.Component {

  render() {

    const {userCounts, onShowDetail} = this.props;

    const loginUserInfo = window._USERINFO_ ? window._USERINFO_ : null;

    return (
      <div style={{ width: "100%", height: "100%", display: "flex" }}>
        <div className={indexStyles.work_card_user_div}>
          <div className={indexStyles.userInfoDiv}>
            <div className={indexStyles.userPhotoDiv}>
              <Avatar size={70} icon={<i className="ri-user-line"></i>} src={userLogo} />
            </div>
            <div className={indexStyles.userFontDiv}>
              <div style={{marginLeft: "5%"}}><span style={{fontWeight: "bold"}}>{loginUserInfo ? loginUserInfo.userName : "admin"}</span>&nbsp;&nbsp;&nbsp;&nbsp;欢迎使用本系统！</div>
              <div style={{marginLeft: "5%"}}>祝你开心每一天！</div>
              <div className={indexStyles.userPhoneAndEmailDiv}>
                <span>{loginUserInfo ? loginUserInfo.phoneNumber : "15555555555"}</span>&nbsp;&nbsp;&nbsp;&nbsp;
                <span>{loginUserInfo ? loginUserInfo.email : "test@test.com"}</span>
              </div>
            </div>
            <div className={indexStyles.allUserInfo}>
              <div onClick={() => onShowDetail("list")} className={indexStyles.fontWeightHover}>
                <Tooltip title={"全部用户"}>
                  <i className="ri-group-line" style={{fontSize: "18px", marginLeft: "70px", marginTop: "5px"}}></i>
                </Tooltip>
              </div>
            </div>
          </div>
        </div>
        <div className={indexStyles.platform_user_notice_message}>
          <Row>
            <Col span={6} />
            <Col span={6} style={{padding: "0px 0px 0px 10px"}}>
              <Card style={{textAlign: "center", background: "#ffffff"}} className={styles.userVisitTotalDiv}>
                <Statistic title="通知"
                           value={userCounts ? userCounts.userCount : ""}
                           precision={0}
                           valueStyle={{color: '#faad14', fontSize: "28px"}}
                           prefix={<i className="ri-registered-fill" style={{marginRight: "10px", verticalAlign: "bottom"}}></i>}
                           suffix="" />
              </Card>
            </Col>
            <Col span={6} style={{padding: "0px 0px 0px 10px"}}>
              <Card style={{textAlign: "center", background: "#ffffff"}} className={styles.userVisitTotalDiv}>
                <Statistic title="待办"
                           value={userCounts ? userCounts.visitCount : ""}
                           precision={0}
                           valueStyle={{color: 'green', fontSize: "25px"}}
                           prefix={<i className="ri-global-line" style={{marginRight: "10px", verticalAlign: "bottom"}}></i>}
                           suffix="" />
              </Card>
            </Col>
            <Col span={6} style={{padding: "0px 0px 0px 10px"}}>
              <Card style={{textAlign: "center", background: "#ffffff"}} className={styles.userVisitTotalDiv}>
                <Statistic title="日程"
                           value={userCounts ? userCounts.visitCount : ""}
                           precision={0}
                           valueStyle={{color: 'red', fontSize: "25px"}}
                           prefix={<i className="ri-calendar-todo-line" style={{marginRight: "10px", verticalAlign: "bottom"}}></i>}
                           suffix="" />
              </Card>
            </Col>
          </Row>
        </div>
      </div>
    );
  };
}

export default UserPlatformCard;
