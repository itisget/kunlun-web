import React from 'react';
import indexStyles from "../../../pages/home/Home.less";
import {Tooltip} from 'antd';
import moment from 'moment';
import styles from "../Home.less";

class WorkLinkCard extends React.Component {

  render() {

    const {onShowDetail} = this.props;

    const linkList = [
      { id: 11111, name: "百度一下", url: "www.baidu.com", color: "#B6F6BFFF", desc: "两款发动机哦额外i就勾搭拉卡萨金佛额我" },
      { id: 11111, name: "科学无止境", url: "www.baidu.com", color: "#96C6F6FF",  desc: "两款发动机哦额外i就勾搭拉卡萨金佛额我" },
      { id: 11111, name: "此生也有涯", url: "www.baidu.com", color: "#F6EF9DFF",  desc: "两款发动机哦额外i就勾搭拉卡萨金佛额我" },
      { id: 11111, name: "开源中国", url: "www.baidu.com", color: "#DCB7F3FF",  desc: "两款发动机哦额外i就勾搭拉卡萨金佛额我" },
      { id: 11111, name: "协同外链", url: "www.baidu.com", color: "#F8D2BDFF",  desc: "两款发动机哦额外i就勾搭拉卡萨金佛额我" },
    ];

    return (
      <div style={{ width: "100%", height: "100%", background: "#ffffff" }}>
        <div className={styles.navigateTitleDiv}>
          <div className={styles.navigateleFontDiv}>协同外链</div>
          <div onClick={() => onShowDetail("navigate")} className={indexStyles.fontWeightHover}>
            <Tooltip title={"自定义导航"}>
              <i className="ri-link-unlink" style={{fontSize: "19px", marginRight: "15px"}}></i>
            </Tooltip>
          </div>
        </div>
        <div className={styles.work_link_card_content}>
          {
            linkList.map(item =>
              <div onClick={() => window.open(item.url)} className={styles.work_link_card_content_item} style={{ background: item.color }}>
                <div style={{ textAlign: "center" }}>{item.name}</div>
                <div className={styles.work_link_card_content_item_desc}>{item.desc}</div>
              </div>
            )
          }
        </div>
      </div>
    );
  };
}

export default WorkLinkCard;
