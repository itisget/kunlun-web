import React from 'react';
import indexStyles from "../../../pages/home/Home.less";
import { Table, Tooltip } from 'antd';
import moment from 'moment';

class TodoAuditCard extends React.Component {

  render() {

    const {onShowDetail} = this.props;

    const todoAuditCardWidth = (window.innerWidth - 60) / 3;
    const dateFormat = "YYYY-MM-DD HH:mm:ss";
    const columns = [
      { title: '待办标题', dataIndex: 'flowName', key: 'flowName', width: todoAuditCardWidth * 0.5, ellipsis: true },
      { title: '岗位类型及处理人', dataIndex: 'nodeName', key: 'nodeName', width: todoAuditCardWidth * 0.2, ellipsis: true,
        render: (text, record, index) => record.nodeName + "/" + record.hanlerName
      },
      { title: '处理完成时间', dataIndex: 'lastDate', key: 'lastDate', width: todoAuditCardWidth * 0.3, ellipsis: true,
        render: (text, record, index) => moment(text).format(dateFormat)
      },
    ];

    const list = [
      {id: "11111", flowName: "请假审批试运行流程", nodeName: "项目经理", hanlerName: "张三", lastDate: new Date() },
      {id: "22222", flowName: "项目发布生产环境审理流程", nodeName: "产品经理", hanlerName: "张三", lastDate: new Date() },
      {id: "33333", flowName: "员工薪资保密制度审批流程", nodeName: "项目经理", hanlerName: "张三", lastDate: new Date() },
      {id: "44444", flowName: "公司高层召开员工扩大会议流程", nodeName: "产品经理", hanlerName: "李四", lastDate: new Date() },
      {id: "55555", flowName: "Spring新技术研讨流程", nodeName: "项目经理", hanlerName: "张三", lastDate: new Date() },
      {id: "66666", flowName: "项目发布生产环境审理流程", nodeName: "项目经理", hanlerName: "张三", lastDate: new Date() },
      {id: "77777", flowName: "公司高层召开员工扩大会议流程", nodeName: "项目经理", hanlerName: "张三", lastDate: new Date() },
    ];

    return (
      <>
        <div className={indexStyles.todo_audit_card_title}>
          <div className={indexStyles.new_notice_title_font}>待办审核</div>
          <div className={indexStyles.new_notice_show}>
            <div onClick={() => onShowDetail("notice")} className={indexStyles.new_notice_show_icon}>
              <Tooltip title={"查看详情"}>
                <i className="ri-briefcase-line" style={{fontSize: "19px", marginRight: "15px"}}></i>
              </Tooltip>
            </div>
          </div>
        </div>
        <div className={indexStyles.todo_audit_card_table}>
          <Table columns={columns} dataSource={list} rowKey={record => record.id} pagination={false} />
        </div>
      </>
    );
  };
}

export default TodoAuditCard;
