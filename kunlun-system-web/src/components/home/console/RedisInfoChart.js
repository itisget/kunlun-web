import React from 'react';
import styles from '../Home.less';
import indexStyles from "../../../pages/home/Home.less";
import { DualAxes, G2 } from '@ant-design/charts';

class RedisInfoChart extends React.Component {

  render() {

    const {redisInfos} = this.props;

    let redisValues = [], redisMemorys = [];
    for (let i = 0; i < redisInfos.length; i++) {
      const item = redisInfos[i];
      if (item.type == "keyValue") {
        redisValues.push({time: item.time, "键值对数": parseInt(item.value)});
      } else if (item.type == "memory") {
        redisMemorys.push({time: item.time, "占用内存": parseFloat(item.value)});
      }
    }

    const registerTheme = G2.registerTheme;
    registerTheme('custom-theme', {
      colors10: ['#ee0606', '#32f307'],
    });

    const config = {
      padding: [15, 18, 13, 18],
      data: [redisMemorys, redisValues],
      xField: 'time',
      yField: ['占用内存', '键值对数'],
      theme: 'custom-theme',
      geometryOptions: [
        {
          geometry: 'column',
          isStack: true,
          columnWidthRatio: 0.4,
        },
        { geometry: 'line' },
      ],
      legend: {
        visible: true,
        flipPage: true,
        offsetX: 18,
      },
    };

    return (
      <>
        <div className={indexStyles.redisTitleDiv}>Redis资源</div>
        <div id={"redisMemory"} className={styles.redisCanvas}>
          <DualAxes {...config} />
        </div>
      </>
    );
  };
}

export default RedisInfoChart;
