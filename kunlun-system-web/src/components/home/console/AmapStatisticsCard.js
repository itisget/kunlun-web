import React from 'react';
import { Map, APILoader } from '@uiw/react-amap';
import config from "../../../config/config";
import amapConfig from "../../../config/amapConfig";

class AmapStatisticsCard extends React.Component {

  render() {

    const {dispatch, zoom, center} = this.props;

    const colors = {};
    const LabelsData = amapConfig.DISTRICt_LABEL;
    const GDPSpeed = amapConfig.GDP_SPEED;
    const getColorByDGP = (adcode) => {
      if (!colors[adcode]) {
        const gdp = GDPSpeed[adcode];
        if (!gdp) {
          colors[adcode] = 'rgb(227, 227, 227)';
        } else {
          var rg = 255 - Math.floor((gdp - 5) / 5 * 255);
          colors[adcode] = 'rgb(' + rg + ',' + rg + ',255)';
        }
      }
      return colors[adcode];
    }

    const handleRenderLayer = (AMap, map) => {
      // 简易行政区
      const layerStyles = {
        "stroke-width": 2,
        'nation-stroke': '#ff0000',
        'coastline-stroke': [0.85, 0.63, 0.94, 1],
        'province-stroke': 'white',
        'city-stroke': 'rgba(255,255,255,0.15)',
      };
      const distCountry = new AMap.DistrictLayer.Country({
        zIndex: 10,
        SOC: 'CHN',
        depth: 1,
        styles: {
          ...layerStyles,
          'fill': (props) => {
            console.log("UserAMapPage layerStyles props => ", props);
            return getColorByDGP(props.adcode);
          }
        }
      });

      // 省级标注
      map.on('complete', function() {
        var layer = new AMap.LabelsLayer();
        for (var i = 0; i < LabelsData.length; i++) {
          var labelsMarker = new AMap.LabelMarker(LabelsData[i]);
          layer.add(labelsMarker);
        }
        map.add(layer);
      });
      map.add(distCountry);
      return;
    };

    return (
      <div id={"appMap"} style={{ width: '100%', height: '100%' }}>
        <APILoader version="2.0.5" akey={config.amap_info.amapkey}>
          <Map
            style={{ height: "calc(100% - 0px)" }}
            center={center}
            zoom={zoom}
            zoomEnable={false}
            dragEnable={false}
            keyboardEnable={false}
          >
            { ({ AMap, map, container }) => handleRenderLayer(AMap, map) }
          </Map>
        </APILoader>
      </div>
    );
  };
}

export default AmapStatisticsCard;
