import React from 'react';
import { Timeline, Tooltip } from 'antd';
import styles from '../Home.less';
import indexStyles from "../../../pages/home/Home.less";
import moment from 'moment';

class OperateLogChart extends React.Component {

  render() {

    const dateFormat = "YYYY-MM-DD HH:mm:ss";

    const logList = [
        { id: 11111, desc: "登录系统", userName: "admin", date: new Date() },
        { id: 22222, desc: "创建菜单", userName: "admin", date: new Date() },
        { id: 33333, desc: "编辑数据", userName: "admin", date: new Date() },
        { id: 44444, desc: "新增任务", userName: "admin", date: new Date() },
        { id: 55555, desc: "编辑任务", userName: "admin", date: new Date() },
    ];

    const createTimeLineItems = () => {
        let timeLineItems = new Array();
        for (let i = 0; i < logList.length; i++) {
            const record = logList[i];
            timeLineItems.push({ label: moment(record.date).format(dateFormat), children: record.userName + " " + record.desc });
        }
        return timeLineItems;
    }

    return (
        <>
          <div className={indexStyles.tableCTitleDiv}>
            <div className={indexStyles.tableCTitleFont}>操作日志</div>
            <div className={indexStyles.tableCTitleTool}>
              <div onClick={() => onShowDetail("zipkin")} className={indexStyles.fontWeightHover}>
                <Tooltip title={"查看详情"}>
                  <i className="ri-swap-box-line" style={{fontSize: "19px", marginRight: "15px"}}></i>
                </Tooltip>
              </div>
            </div>
          </div>
          <div id={"operateLog"} className={styles.operate_log_chart_show}>
            <Timeline mode={'left'} items={ createTimeLineItems() } />
          </div>
        </>
    );
  };
}

export default OperateLogChart;
