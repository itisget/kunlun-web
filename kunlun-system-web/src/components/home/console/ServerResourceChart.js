import React from 'react';
import { Tooltip } from 'antd';
import styles from '../Home.less';
import indexStyles from "../../../pages/home/Home.less";
import { Gauge } from '@ant-design/charts';

class ServerResourceChart extends React.Component {

  render() {

    const cpuConfig = {
      percent: 0.35,
      padding: [5, 0, 5, 0],
      radius: 1,
      innerRadius: 0.8,
      range: {
        ticks: [0, 1],
        color: ['l(0) 0:#30bf78 0.5:#a0d911 0.7:#faad14 1:#f4664a'],
      },
      indicator: {
        pointer: { style: { stroke: '#D0D0D0' } },
        pin: { style: { stroke: '#D0D0D0' } },
      },
      statistic: {
        content: {
          offsetY: 15,
          style: {
            fontSize: '14px',
            color: '#4B535E',
          },
          formatter: function formatter() {
            return 'CPU使用率';
          },
        },
      },
    };

    const memoryConfig = {
      radius: 1,
      percent: 0.54,
      type: 'meter',
      innerRadius: 0.8,
      padding: [5, 0, 5, 0],
      range: {
        ticks: [0, 1],
        color: ['l(0) 0:#30bf78 0.5:#a0d911 0.7:#faad14 1:#f4664a'],
      },
      indicator: {
        pointer: { style: { stroke: '#D0D0D0' } },
        pin: { style: { stroke: '#D0D0D0' } },
      },
      statistic: {
        content: {
          offsetY: 15,
          style: {
            fontSize: '14px',
            color: '#4B535E',
          },
          formatter: function formatter() {
            return '内存使用率';
          },
        },
      },
    };

    const diskConfig = {
      percent: 0.25,
      padding: [5, 0, 5, 0],
      radius: 1,
      innerRadius: 0.8,
      range: {
        ticks: [0, 1],
        color: ['l(0) 0:#30bf78 0.5:#a0d911 0.7:#faad14 1:#f4664a'],
      },
      indicator: {
        pointer: { style: { stroke: '#D0D0D0' } },
        pin: { style: { stroke: '#D0D0D0' } },
      },
      statistic: {
        content: {
          offsetY: 15,
          style: {
            fontSize: '14px',
            color: '#4B535E',
          },
          formatter: function formatter() {
            return '磁盘使用率';
          },
        },
      },
    };

    return (
      <>
        <div className={indexStyles.tableCTitleDiv}>
          <div className={indexStyles.tableCTitleFont}>服务器资源</div>
          <div className={indexStyles.tableCTitleTool}>
            <div onClick={() => onShowDetail("zipkin")} className={indexStyles.fontWeightHover}>
              <Tooltip title={"查看详情"}>
                <i className="ri-funds-box-line" style={{fontSize: "19px", marginRight: "15px"}}></i>
              </Tooltip>
            </div>
          </div>
        </div>
        <div className={styles.serverMemoryDiv}>
          <div id={"serverCpu"} className={styles.serverCPU}>
            <Gauge {...cpuConfig} />
          </div>
          <div id={"serverMemory"} className={styles.serveMemory}>
            <Gauge {...memoryConfig} />
          </div>
          <div id={"serverDisk"} className={styles.serverDisk}>
            <Gauge {...diskConfig} />
          </div>
        </div>
      </>
    );
  };
}

export default ServerResourceChart;
