import React from 'react';
import styles from '../Home.less';
import indexStyles from "../../../pages/home/Home.less";
import { Pie  } from '@ant-design/charts';

class BrowseChart extends React.Component {

  render() {

    const data = [
      { type: 'Chrome', value: 60 },
      { type: 'Safari', value: 15 },
      { type: 'Firefox', value: 10 },
      { type: 'Edge', value: 7 },
      { type: 'IE', value: 5 },
      { type: 'Other', value: 3 },
    ];

    const config = {
      data,
      radius: 1,
      angleField: 'value',
      colorField: 'type',
      padding: [10, 10, 10, -140],
      label: {
        type: 'inner',
        offset: '-30%',
        content: ({ percent }) => `${(percent * 100).toFixed(0)}%`,
        style: {
          fontSize: 14,
          textAlign: 'center',
        },
      },
      legend: {
        visible: true,
        offsetX: -70,
      },
      interactions: [
        {
          type: 'element-active',
        },
      ],
    };

    return (
        <>
          <div className={indexStyles.tableBTitleDiv}>浏览器分布</div>
          <div id={"serverCluster"} className={styles.browse_visit_canvas}>
            <Pie  {...config} />
          </div>
        </>
    );
  };
}

export default BrowseChart;
