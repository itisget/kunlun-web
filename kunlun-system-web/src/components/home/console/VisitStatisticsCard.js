import React from 'react';
import {Card, Col, Row, Statistic, Tooltip} from 'antd';
import indexStyles from "../../../pages/home/Home.less";
import styles from '../Home.less';

class VisitStatisticsCard extends React.Component {

  render() {

    const {userCounts, onShowDetail} = this.props;

    return (
      <div style={{ width: "100%", height: "100%", background: "#ffffff" }}>
        <Row>
          <Col span={23}>
            <Row>
              <Col span={6}>
                <Card style={{textAlign: "center", background: "#ffffff"}} className={styles.userVisitTotalDiv}>
                  <Statistic title="注册用户数"
                             value={userCounts ? userCounts.userCount : ""}
                             precision={0}
                             valueStyle={{color: '#faad14', fontSize: "28px"}}
                             prefix={<i className="ri-registered-fill" style={{marginRight: "10px", verticalAlign: "bottom"}}></i>}
                             suffix="" />
                </Card>
              </Col>
              <Col span={6} style={{padding: "0px 0px 0px 10px"}}>
                <Card style={{textAlign: "center", background: "#ffffff"}} className={styles.userVisitTotalDiv}>
                  <Statistic title="总访问量"
                             value={userCounts ? userCounts.visitCount : ""}
                             precision={0}
                             valueStyle={{color: 'green', fontSize: "25px"}}
                             prefix={<i className="ri-global-line" style={{marginRight: "10px", verticalAlign: "bottom"}}></i>}
                             suffix="" />
                </Card>
              </Col>
              <Col span={6} style={{padding: "0px 0px 0px 10px"}}>
                <Card style={{textAlign: "center", background: "#ffffff"}} className={styles.userOnlineDiv}>
                  <Statistic title="在线人数"
                             value={userCounts ? userCounts.onlineCount : ""}
                             precision={0}
                             valueStyle={{color: 'red', fontSize: "25px"}}
                             prefix={<i className="ri-macbook-fill" style={{marginRight: "10px", verticalAlign: "bottom"}}></i>}
                             suffix="" />
                </Card>
              </Col>
              <Col span={6} style={{padding: "0px 0px 0px 10px"}}>
                <Card style={{textAlign: "center", background: "#ffffff"}} className={styles.userVisitLastDiv}>
                  <Statistic title="近一个月访问量"
                             value={userCounts ? userCounts.leastCount : ""}
                             precision={0}
                             valueStyle={{color: 'blue', fontSize: "25px"}}
                             prefix={<i className="ri-computer-fill" style={{marginRight: "10px", verticalAlign: "bottom"}}></i>}
                             suffix="" />
                </Card>
              </Col>
            </Row>
          </Col>
          <Col span={1}>
            <div className={indexStyles.userCountBlock}>
              <div style={{float: "right"}} onClick={() => onShowDetail("online")} className={indexStyles.fontWeightHover}>
                <Tooltip title={"登录详情"}>
                  <i className="ri-user-line" style={{fontSize: "18px", marginRight: "15px"}}></i>
                </Tooltip>
              </div>
            </div>
          </Col>
        </Row>
      </div>
    );
  };
}

export default VisitStatisticsCard;
