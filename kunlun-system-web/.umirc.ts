import { defineConfig } from 'umi';

export default defineConfig({
  dva: {},
  fastRefresh: true,
  routes: [
    {
      // 我的地盘/工作空间
      path: "/home/platform", component: "@/pages/home/PlatformPage"
    }, {
      // 我的地盘/监控平台
      path: "/home/console", component: "@/pages/home/ConsolePage"
    }, {
      // 我的地盘/个人信息
      path: "/home/person", component: "@/pages/home/PersonPage"
    }, {
      // 用户管理/单位管理
      path: "/user/company", component: "@/pages/user/CompanyPage"
    }, {
      // 用户管理/部门管理
      path: "/user/department", component: "@/pages/user/DepartmentPage"
    }, {
      // 用户管理/岗位管理
      path: "/user/post", component: "@/pages/user/WorkPostPage"
    }, {
      // 用户管理/人员用户
      path: "/user/list", component: "@/pages/user/UserPage"
    }, {
      // 用户管理/关联管理
      path: "/user/correlate", component: "@/pages/user/CorrelatePage"
    }, {
      // 用户管理/授权管理
      path: "/user/authorize", component: "@/pages/user/AuthorizePage"
    }, {
      // 用户管理/在线用户
      path: "/user/online", component: "@/pages/user/OnlinePage"
    }, {
      // 用户管理/用户地图
      path: "/user/amap", component: "@/pages/user/UserAMapPage"
    }, {
      // 协同管理/模型管理
      path: "/synergy/model", component: "@/pages/synergy/ModelPage"
    }, {
      // 协同管理/流程管理
      path: "/synergy/process", component: "@/pages/synergy/ProcessPage"
    }, {
      // 协同管理/待办任务
      path: "/synergy/todo", component: "@/pages/synergy/ProcessPage"
    }, {
      // 协同管理/操作日志
      path: "/synergy/log", component: "@/pages/synergy/OperatorLogPage"
    }, {
      // 协同管理/事项日程
      path: "/synergy/schedule", component: "@/pages/synergy/SchedulePage"
    }, {
      // 资源管理/外链管理
      path: "/resource/link", component: "@/pages/resource/LinksPage"
    }, {
      // 资源管理/监控统计
      path: "/resource/monitor", component: "@/pages/resource/MonitorPage"
    }, {
      // 资源管理/环境监控
      path: "/resource/virtual", component: "@/pages/resource/MachinePage"
    }, {
      // 资源管理/图片管理
      path: "/resource/picture", component: "@/pages/resource/PicturePage"
    }, {
      // 系统管理/菜单管理
      path: "/option/menu", component: "@/pages/option/MenuPage"
    }, {
      // 系统管理/图标管理
      path: "/option/icon", component: "@/pages/option/IconPage"
    }, {
      // 系统管理/数据字典
      path: "/option/dictionary", component: "@/pages/option/DictionaryPage"
    }, {
      // 系统管理/导航管理
      path: "/option/navigate", component: "@/pages/option/NavigationPage"
    }, {
      // 系统管理/通知公告
      path: "/option/notice", component: "@/pages/option/MessagePage"
    }, {
      // 系统管理/了解系统
      path: "/option/info", component: "@/pages/option/SystemInfoPage"
    }
  ],
});
